#!/bin/bash

cd $1

mkdir -p logs

ls -1 "pending_jobs" | \
    while read job; do
        if $(mv "pending_jobs/$job" "running_jobs" 2>/dev/null); then
            echo "Starting $job"
            touch "running_jobs/$job"
            bash "running_jobs/$job" 2>&1 | tee "logs/${job%.sh}.log"
            if [ ${PIPESTATUS[0]} -ne 0 ]; then
                echo "Job $job failed"
                mv "running_jobs/$job" "failed_jobs"
                touch "failed_jobs/$job"
            else
                echo "Finished $job"
                mv "running_jobs/$job" "finished_jobs"
                touch "finished_jobs/$job"
            fi
        fi
    done

