package swc.data;

import edu.cmu.sphinx.util.TimeFrame;

/**
 * An Annotation for Audio files.
 * Consists of a Timeframe and a Label for that TimeFrame,
 * can be used for Lab files to add transcriptions.
 */
public interface Annotation {

    public String getLabel();
    public TimeFrame getTimeFrame();

    default boolean hasTiming() {
        return getTimeFrame() != null;
    }
}
