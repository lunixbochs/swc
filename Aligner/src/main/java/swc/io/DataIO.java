package swc.io;

import edu.cmu.sphinx.util.TimeFrame;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swc.data.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataIO {

	public static final Logger logger = LoggerFactory.getLogger(DataIO.class);

	public static void writeLegacyJsonFile(Document doc, OutputStream out) {
		SwcXmlUtil sxu = new SwcXmlUtil(doc);
		List<Element> tokens = sxu.getTokens();

		// create datastructure
		JSONObject data = new JSONObject();
		JSONArray words = new JSONArray();
		data.put("words", words);
		JSONArray sentences = new JSONArray();
		data.put("sentences", sentences);

		Element currentSentence = null;
		JSONObject currentSentenceJson = null;
		int index = 0;
		for (Element t : tokens) {
			Element sentence = sxu.getSentenceLikeAncestor(t);
			if (sentence != currentSentence) {
				if (currentSentence != null) {
					currentSentenceJson.put("end", index);
					sentences.put(currentSentenceJson);
					currentSentenceJson = null;
				}
				currentSentence = sentence;
				if (sentence != null) { // create Json object for sentence with initial index
					currentSentenceJson = new JSONObject();
					currentSentenceJson.put("start", index);
				}
			}
			JSONObject word = new JSONObject();
			word.put("original", sxu.getTokenText(t));
			word.put("normalized", String.join(" ", sxu.getTokenTextNormalized(t)));
			word.put("index", index);
			TimeFrame tf = sxu.getAggregateTiming(t);  // Might be inaccurate
			word.put("start", tf != null ? tf.getStart() : (JSONObject)null);
			word.put("stop", tf != null ? tf.getEnd() : (JSONObject)null);
			words.put(word);

			index++;
		}

		// Write the datastructure to the outputstream
		PrintWriter writer = new PrintWriter(out);
		writer.print(data.toString(2));
		writer.flush();
	}
	
	
	/**
	 * Output of the complete datastructure as XML, contains all available information.
	 */
	public static void writeDocumentToXml(swc.data.Document doc, OutputStream outstream) throws IOException {
		Element root = new Element("d");
		for (Paragraph p : doc) {
			Element paragraph = new Element("p");
			for (Sentence s : p) {
				Element sentence = new Element("s");
				for (Word w : s) {
					Element word = new Element("t");
					word.setAttribute("orig", w.original);
					for (Token t : w.parts) {
						Element token = new Element("n");
						token.setAttribute("spelling", t.getSpelling());
						if (t.getTimeFrame() != null) {
							token.setAttribute("start", new Long(t.getTimeFrame().getStart()).toString());
							token.setAttribute("end", new Long(t.getTimeFrame().getEnd()).toString());
						}
						word.addContent(token);
					}
					sentence.addContent(word);
				}
				paragraph.addContent(sentence);
			}
			root.addContent(paragraph);
		}
		Document xmlDoc = new Document(root);
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		outputter.output(xmlDoc, outstream);
	}
	
	public static swc.data.Document readDocument(InputStream instream) throws JDOMException, IOException {
		SAXBuilder jdomBuilder = new SAXBuilder();
		//Document xmlDoc = jdomBuilder.build(new File(filename));
		Document xmlDoc = jdomBuilder.build(instream);
		Element root = xmlDoc.getRootElement();

		swc.data.Document doc = new swc.data.Document();
		for (Element p : root.getChildren("p")) {
			Paragraph paragraph = new Paragraph();
			for (Element s : p.getChildren("s")) {
				Sentence sentence = new Sentence();
				for (Element t : s.getChildren("t")) {
					Word word = new Word();
					word.parts = new ArrayList<>();
					word.original = t.getAttributeValue("orig");
					for (Element n : t.getChildren("n")) {
						Token token = new Token(n.getAttributeValue("spelling"));
						if (n.getAttribute("start") != null) {
							long start = Long.parseLong(n.getAttributeValue("start"));
							long end   = Long.parseLong(n.getAttributeValue("end"));
							token.setTimeFrame(new TimeFrame(start, end));
						}
						word.parts.add(token);
					}
					sentence.add(word);
				}
				paragraph.add(sentence);
			}
			doc.add(paragraph);
		}
		return doc;
	}

	/**
	 * For each normalized token in the document, that also has a timing,
	 * a label is created in the lab file.
	 */
	public static void writeNormalizedTokensLabFile(Document doc, OutputStream outputStream) {
		SwcXmlUtil sxu = new SwcXmlUtil(doc);
		List<Annotation> annotations = sxu.getNormalizedTokens().stream()
				.filter(sxu::hasCompleteTiming)
				.map(sxu::toAnnotation)
				.collect(Collectors.toList());
		writeLabFile(annotations, outputStream);
	}

	/**
	 * a label for each maused phone in the document
	 */
	public static void writeNormalizedPhonesLabFile(Document doc, OutputStream outputStream) {
		SwcXmlUtil sxu = new SwcXmlUtil(doc);
		List<Annotation> annotations = sxu.getPhones().stream()
				.map(sxu::toAnnotation)
				.collect(Collectors.toList());
		writeLabFile(annotations, outputStream);
	}

	/**
	 * Creates Annotations for each time frame with the label "speech"
	 */
	public static void writeVadLabFile(List<TimeFrame> vadTimeFrames, OutputStream outputStream) {
		// Convert TimeFrames to Annotations where the label is always "speech"
		List<Annotation> annotations = vadTimeFrames.stream()
				.map(tf -> new SimpleAnnotation(tf, "speech"))
				.collect(Collectors.toList());
		// Write the new annotations to file
		writeLabFile(annotations, outputStream);
	}

	public static void writeEnumerationLabFile(List<TimeFrame> timeFrames, String format, OutputStream outputStream) {
		logger.debug(String.format("Writing Enumeration .lab file with %d timeframes", timeFrames.size()));
		List<Annotation> annotations = IntStream.range(0, timeFrames.size())
				.mapToObj(i -> new SimpleAnnotation(timeFrames.get(i), String.format(format, i)))
				.collect(Collectors.toList());
		logger.debug(String.format("Writing Enumeration .lab file with %d annotations", annotations.size()));
		for (Annotation a : annotations)
			logger.debug(String.format("Annotation: %d %d %s", a.getTimeFrame().getStart(), a.getTimeFrame().getEnd(), a.getLabel()));
		writeLabFile(annotations, outputStream);
	}

	public static void writeLabFile(List<? extends Annotation> annotations, OutputStream outputStream) {
		// assumes the annotations are sorted by their TimeFrames.
		PrintWriter writer = new PrintWriter(outputStream);
		for (Annotation a : annotations) {
			double s = a.getTimeFrame().getStart() / 1000.0;
			double e = a.getTimeFrame().getEnd() / 1000.0;
			String l = a.getLabel();
			writer.println(String.format(Locale.US, "%.3f %.3f %s", s, e, l));
		}
		writer.flush();
	}
}
