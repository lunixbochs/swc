﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace WikiDownloader
{
    class DateParser
    {

        public static DateTime? ParseDate(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return null;
            }

            DateTime? date = ParseDateOnly(str);
            if(date != null)
            {
                var matches = Regex.Matches(str, @"(?:^|[^\d])(\d\d?):(\d\d?)(?::(\d\d?))?(?:\s*(pm)(?:[^\w]|$)|[^\d]|$)");// "8:10:59", "11:30 pm", etc
                Match match = matches.Cast<Match>().OrderByDescending(m => m.Length).FirstOrDefault();
                if(match!= null && match.Success)
                {
                    int hours = int.Parse(match.Groups[1].Value);
                    int minutes = int.Parse(match.Groups[2].Value);
                    int seconds;
                    if (match.Groups[3].Length > 0)
                        seconds = int.Parse(match.Groups[3].Value);
                    else
                        seconds = 0;

                    if (hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60 && seconds >= 0 && seconds < 60)
                    {
                        bool pm =
                            match.Groups.Count > 4 &&
                            match.Groups[4].Success &&
                            !string.IsNullOrWhiteSpace(match.Groups[4].Value);

                        if (pm)
                        {
                            hours += 12;
                        }

                        date = new DateTime(date.Value.Year, date.Value.Month, date.Value.Day, hours, minutes, seconds);
                    }
                }
                return date;
            }

            DateTime result;
            if (DateTime.TryParse(str, out result))
                return result;

            Console.Error.WriteLine("ERROR: Can't parse "+str+" as date");
            return null;
        }

        enum Month
        {
            January = 1,
            Jan = 1,
            Januar = 1,
            February = 2,
            Feb = 2,
            Februar = 2,
            March = 3,
            Mar = 3,
            März = 3,
            April = 4,
            Apr = 4,
            May = 5,
            Mai = 5,
            June = 6,
            Jun = 6,
            Juni = 6,
            July = 7,
            Jul = 7,
            Juli = 7,
            August = 8,
            Aug = 8,
            September = 9,
            Sep = 9,
            October = 10,
            Oct = 10,
            Oktober = 10,
            Okt = 10,
            November = 11,
            Nov = 11,
            December = 12,
            Dec = 12,
            Dez = 12,
            Dezember = 12
        }

        private static DateTime? ParseDateOnly(string str)
        {
            try
            {
                Match m;
                m = Regex.Match(str, @"(?:^|[^\d])(\d\d\d\d)-(\d\d?)-(\d\d?)(?:$|[^\d])");
                if (m.Success)//2008-01-02
                {
                    int year = int.Parse(m.Groups[1].Value);
                    int month = int.Parse(m.Groups[2].Value);
                    int day = int.Parse(m.Groups[3].Value);
                    return new DateTime(year, month, day);
                }
                m = Regex.Match(str, @"(?:^|[^\d])(\d\d?)\s*(?:nd|st|rd)(?:\s+of)?[^\w]*(\w+)[^\w]+(\d\d\d\d)(?:$|[^\d])");
                if (m.Success && ParseMonth(m.Groups[2].Value) > 0)//2nd of Jan 2008
                {
                    int year = int.Parse(m.Groups[3].Value);
                    int month = ParseMonth(m.Groups[2].Value);
                    int day = int.Parse(m.Groups[1].Value);
                    return new DateTime(year, month, day);
                }
                m = Regex.Match(str, @"(?:^|[^\w])(\w+)[^\w]+(\d\d?)\s*(?:nd|st|rd)[^w]+(\d\d\d\d)(?:$|[^d])");
                if (m.Success && ParseMonth(m.Groups[1].Value) > 0)//Jan 2nd, 2008
                {
                    int year = int.Parse(m.Groups[3].Value);
                    int month = ParseMonth(m.Groups[1].Value);
                    int day = int.Parse(m.Groups[2].Value);
                    return new DateTime(year, month, day);
                }
                m = Regex.Match(str, @"(?:^|[^\d])(\d\d?)[^\w/]+(\w+)[^\w/]+(\d\d\d\d)(?:$|[^\d])");
                if (m.Success && ParseMonth(m.Groups[2].Value) > 0)//29 March 2008
                {
                    int year = int.Parse(m.Groups[3].Value);
                    int month = ParseMonth(m.Groups[2].Value);
                    int day = int.Parse(m.Groups[1].Value);
                    return new DateTime(year, month, day);
                }
                m = Regex.Match(str, @"(?:^|[^\d])(20\d\d)(\d\d)(\d\d)(?:$|[^\d])");//20080102
                if (m.Success)
                {
                    int year = int.Parse(m.Groups[3].Value);
                    int month = int.Parse(m.Groups[2].Value);
                    int day = int.Parse(m.Groups[1].Value);
                    return new DateTime(year, month, day);
                }
            }
            catch(ArgumentOutOfRangeException) { }
            return null;
        }

        private static int ParseMonth(string p)
        {
            if (string.IsNullOrWhiteSpace(p))
                return -1;
            p = p.Trim();
            try
            {
                return (int)Enum.Parse(typeof(Month), p);
            }
            catch { }
            int result;
            if (int.TryParse(p, out result) && result >= 1 && result <= 12)
                return result;
            return -1;
        }
    }
}
