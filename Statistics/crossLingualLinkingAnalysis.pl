#!/usr/bin/perl -I /informatik2/nats/home/baumann/perl5/lib/perl5
use strict;
use warnings;
use JSON;
#usage: put path to L1 and L2 directories and language codes (each containing subdirs named after the article with wiki.txt in them)
# i.e.: crossLingualLinkingAnalysis.pl run_2016-06-11_german/articles/ de run_2016-06-11_english/articles/ en

my ($L1DIR, $L1, $L2DIR, $L2) = @ARGV;
my @l1files = split "\n", `ls $L1DIR/*/wiki.txt`;
print join "\n", @l1files;
print "\n";
my @l2files = split "\n", `ls $L2DIR/*/wiki.txt`;
my %l2articles;
map { $_ =~ m"^$L2DIR/(.*)/wiki.txt$"; $l2articles{$1} = 1; } @l2files;
print join "\n", keys %l2articles;
foreach my $wikitext (@l1files) {
	open FILE, '<', $wikitext;
	while (my $articleText = <FILE>) {
		while ($articleText =~ m"\[\[$L2\:(.*?)[\]\|]"g) {
			my $link = $1;
			$link =~ s/ /_/g;
			print "$wikitext -> $link\n" if (defined $l2articles{$link});
		}
	}
	close FILE;
}
