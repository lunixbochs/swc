#!/usr/bin/env python3
import os
import os.path
import re
import subprocess
import sys
import json
from decimal import Decimal
from collections import OrderedDict

## call: ./prepare_audio.py <wiki_article_directory>
##
## reads the audio.ogg or audio1.ogg, audio2.ogg ... for the directory
## and creates the audio.wav with the correct format for use with CMUSphinx
##
## requires SoX 


def eprint(*args, **kwargs):
    """print to stderr"""
    print(*args, file=sys.stderr, **kwargs)

def call_sox(argslist):
    call_external(["sox"] + argslist)

def duration(audio_file):
    value = call_external(["soxi", "-D", audio_file])
    return float(value)

def call_external(call_list):
    eprint("executing '{}'".format(" ".join(call_list)))
    try:
        output = subprocess.check_output(call_list)
    except OSError as e:
        if e.errno == os.errno.EN0ENT:
            eprint("Error: SoX is missing, please install it ('apt install sox')")
            sys.exit(1)
        else:
            eprint("An error occured with SoX, exiting")
            sys.exit(e)
    return output

# audio format requirements for CMUSphinx
out_format_opts = ["--bits", "16",
                   "--endian", "little",
                   "--channels", "1",
                   "--encoding", "signed-integer",
                   "--rate", "16000",
                   "--show-progress"]
# highpass against DC Offset
out_processing_opts = ["highpass", "10"]

# input and output directory
dir = sys.argv[1]

input_files = [os.path.join(dir, "audio.ogg")]
output_file = os.path.join(dir, "audio.wav")
info_file = os.path.join(dir, "info.json")

if not os.path.isfile(input_files[0]):
    eprint("audio.ogg not found, assuming multiple audio files")
    input_files = [os.path.join(dir, f) for f in os.listdir(dir)
                   if re.fullmatch("audio[0-9]+\.ogg", f)]
    input_files = sorted(input_files)

output_files = []
prior_audio_offset = 0.0
audio_offsets = []
for input_file in input_files:
    filename, _ = os.path.splitext(input_file)
    output_file = filename + ".wav"
    call_list = [input_file] + out_format_opts + [output_file] + out_processing_opts
    call_sox(call_list)
    audio_offsets.append(prior_audio_offset)
    prior_audio_offset += duration(output_file)
    output_files.append(output_file)

info_json = {}
with open(info_file) as f:
    info_json = json.load(f, object_pairs_hook=OrderedDict)
print(info_json)
assert len(info_json["audio_files"]) == len(audio_offsets), "audio files and info.json disagree: " + str(len(info_json["audio_files"]))
for i in range(len(audio_offsets)):
    info_json["audio_files"][i]["offset"] = audio_offsets[i]
with open(info_file, 'w') as f:
    json.dump(info_json, f, separators=(',', ':'))


# if we have more than 1 file, concatenate and delete intermediate files
if len(output_files) > 1:
    call_sox(output_files + [os.path.join(dir, "audio.wav")])
    for f in output_files:
        os.remove(f)


