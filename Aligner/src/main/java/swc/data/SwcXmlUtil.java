package swc.data;

import edu.cmu.sphinx.util.TimeFrame;
import org.jdom2.*;
import org.jdom2.Document;
import org.jdom2.filter.Filter;
import org.jdom2.filter.Filters;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Utility Class which works directly on the XML structure, but abstracts from it.
 * Provides String constants for all the XML tags and attributes.
 * Provides methods for accessing parts of the document tree, while acknowledging
 * the 'ignore' tag.
 */
public class SwcXmlUtil {

    private static final String ROOT_TAG = "article";
    private static final String META_TAG = "meta";
    public  static final String META_PROP_TAG = "prop";
    public  static final String META_LINK_TAG = "link";
    private static final String META_KEY_ATTR = "key";
    private static final String META_VAL_ATTR = "value";
    private static final String META_GROUP_ATTR = "group";
    public static final String DOC_TAG = "d";
    public static final String SECTION_TAG = "section";
    public static final String LEVEL_ATTR = "level";
    public static final String SECTIONTITLE_TAG = "sectiontitle";
    public static final String SECTIONCONTENT_TAG = "sectioncontent";
    public static final String PARAGRAPH_TAG = "p";
    public static final String SENTENCE_TAG = "s";  // TODO make private
    public static final String TOKEN_TAG = "t";
    public static final String PRONUNCIATION_TAG = "n";
    public static final String PHONE_TAG = "ph";
    public static final String PRONUNCIATION_ATTR = "pronunciation";
    public static final String SPEECH_START_ATTR = "start";
    public static final String SPEECH_END_ATTR = "end";
    public static final String PHONE_TYPE_ATTR = "type";
    public static final String IGNORE_TAG = "ignored";
    private static final String EXTRA_TAG = "extra";
    private static final String TEXT_ATTR = "text";
    public static final int SECTION_FIRST_LEVEL = 2;
    @SuppressWarnings("unchecked")
	private static final Filter<Element> SENTENCE_LIKE_FILTER = (Filter<Element>)Filters.element(SENTENCE_TAG).or(Filters.element(SECTIONTITLE_TAG));


    private final org.jdom2.Document doc;
    private Filter<? extends Content> subtreeFilter;


    public SwcXmlUtil() {
        this(new Document());
        Element root = new Element(ROOT_TAG);
        doc.setRootElement(root);
        root.addContent(new Element(META_TAG));
        root.addContent(new Element(DOC_TAG));
    }

    public SwcXmlUtil(org.jdom2.Document doc) {
        this(doc, false);
    }

    /**
     *
     * @param doc
     * @param overrideIgnore  If overrideIgnore == true, content inside of ignore tags is still returned
     *                        when for example the method to get all sections is called.  Otherwise, the ignored
     *                        sections are not returned.
     *                        Overriding the ignore tags can be useful when for example the whole document should be
     *                        tokenized, even though some parts of the document are ignored when read out.
     */
    @SuppressWarnings("unchecked")
    public SwcXmlUtil(org.jdom2.Document doc, boolean overrideIgnore) {
        assert doc != null;
        this.doc = doc;
        if (overrideIgnore) {
            subtreeFilter = Filters.content();
        } else {
        	
            subtreeFilter = (Filter<? extends Content>)Filters.element(IGNORE_TAG).negate();
        }
    }

    public Document getDocument() {
        return doc;
    }

    /**
     * Returns the root element of the 'body' of the article, which means
     * the actual content.  The structure is:
     * - article
     *   - meta
     *   - d
     *
     * The 'd' tag is returned
     */
    public Element getBodyRoot() {
        return doc.getRootElement().getChild(DOC_TAG);
    }

    public List<Text> getCDATA() {
        return getDescendants(doc.getRootElement(), Filters.text());
    }

    //<editor-fold desc="Meta related methods">
    private Element getMetaRoot() {
        return doc.getRootElement().getChild(META_TAG);
    }

    public void addMetaProp(String key, String value) {
        addMetaEntry(META_PROP_TAG, key, value, null);
    }

    public void addMetaProp(String key, String value, String group) {
        addMetaEntry(META_PROP_TAG, key, value, group);
    }

    public void addMetaLink(String key, String value) {
        addMetaEntry(META_LINK_TAG, key, value, null);
    }

    public void addMetaLink(String key, String value, String group) {
        addMetaEntry(META_LINK_TAG, key, value, group);
    }

    /**
     * Adds the given information as meta data, as well as the date and git information
     * @param step
     * @param options
     */
    public void addProcessingStepInfo(String step, String options) {
        addMetaProp("processing.step", step, step);
        addMetaProp("processing.step.date", ZonedDateTime.now().toString(), step);
        addMetaProp("processing.step.options", options, step);

        // git version stuff
        Properties props = new Properties();
        try {
            props.load(getClass().getClassLoader().getResourceAsStream("git.properties"));
        } catch (IOException e) {
            throw new IllegalStateException("git.properties file missing in packaged jar.");
        }
        addMetaProp("processing.step.git.commit.id", props.getProperty("git.commit.id"), step);
        addMetaProp("processing.step.git.commit.time", props.getProperty("git.commit.time"), step);
    }

    /**
     * Creates an XML node and adds it to the meta section of the document.
     * If the value is null, nothing is added.
     * @param type XML node name. Either 'prop' or 'link'
     * @param key Anything. E.g. 'DC.source.audio.page' or 'reader.name'
     * @param value Anything. E.g. 'www.xyz.com' or 'JohnDoe'
     * @param groupId null, or an identifier to group multiple meta properties.
     *                Useful when using keys multiple times, i.e. for multiple audio files.
     */
    public void addMetaEntry(String type, String key, String value, String groupId) {
        assert META_PROP_TAG.equals(type) || META_LINK_TAG.equals(type);
        if (value == null)
            return;
        Element e = new Element(type);
        e.setAttribute(META_KEY_ATTR, key);
        e.setAttribute(META_VAL_ATTR, value);
        if (groupId != null)
            e.setAttribute(META_GROUP_ATTR, groupId);
        getMetaRoot().addContent(e);
    }

    public String getArticleTitle() {
        Optional<Element> titleElement = getMetaRoot().getChildren(META_PROP_TAG).stream()
                .filter(elem -> elem.getAttributeValue(META_KEY_ATTR).equals("DC.title")).findFirst();
        if (!titleElement.isPresent())
            throw new IllegalStateException("Title is missing");
        return titleElement.get().getAttributeValue(META_VAL_ATTR);
    }

    /**
     * returns the reader or an ID based on the title if no reader info available
     */
    public String getReader() {
        Optional<Element> readerElement = getMetaRoot().getChildren(META_PROP_TAG).stream()
                .filter(elem -> elem.getAttributeValue(META_KEY_ATTR).equals("reader.name")).findFirst();
        String reader = "";
        if (readerElement.isPresent())
            reader = readerElement.get().getAttributeValue(META_VAL_ATTR);
        if (reader.equals(""))
            reader = "unknown_reader_" + getArticleTitle();
        return reader;
    }

    //</editor-fold>

    //<editor-fold desc="Section related methods">
    public List<Element> getSections(int sectionLevel) {
        List<Element> allSections = getNodes(Filters.element(SECTION_TAG));
        return allSections.stream().filter(e -> getSectionLevel(e) == sectionLevel).collect(Collectors.toList());
    }
    
    public List<Element> getPhones() {
    	return getNodes(Filters.element(PHONE_TAG));
    }

    /**
     * Returns the sentence which contains the given element, or null,
     * if this element is not part of any sentence.
     */
    public Element getSentenceLikeAncestor(Element elem) {
        Element parent;
        do  {
            parent = elem.getParentElement();
            elem = parent;
        } while (parent != null && !SENTENCE_LIKE_FILTER.matches(parent));
        return parent;
    }

    private static int getSectionLevel(Element section) {
        assert SECTION_TAG.equals(section.getName());
        assert section.getAttributeValue(LEVEL_ATTR) != null;
        return Integer.parseInt(section.getAttributeValue(LEVEL_ATTR));
    }

    public String getSectionTitle(Element section) {
        assert SECTION_TAG.equals(section.getName());
        List<Text> texts = getDescendants(section.getChild(SECTIONTITLE_TAG), Filters.text(), subtreeFilter);
        return texts.stream().map(Text::getText).collect(Collectors.joining()).trim();
    }
    //</editor-fold>

    //<editor-fold desc="Sentence related methods">
    public List<Element> getSectionTitles() {
        return getNodes(Filters.element(SECTIONTITLE_TAG));
    }

    public List<Element> getSentenceLikeElements() { return getNodes(SENTENCE_LIKE_FILTER); }
    //</editor-fold>

    //<editor-fold desc="Token (t & n tokens) related methods">
    public List<Element> getTokens() {
        return getNodes(Filters.element(TOKEN_TAG));
    }

    public String getTokenText(Element token) {
        assert TOKEN_TAG.equals(token.getName());
        List<Text> texts = token.getContent(Filters.text());
        return texts.stream().map(Text::getText).collect(Collectors.joining()).trim();
    }

    public List<String> getTokenTextNormalized(Element token) {
        assert TOKEN_TAG.equals(token.getName());
        return getDescendants(token, Filters.element(PRONUNCIATION_TAG)).stream()
                .map(p -> p.getAttributeValue(PRONUNCIATION_ATTR))
                .collect(Collectors.toList());
    }

    public List<Element> getNormalizedTokens() {
        return getNodes(Filters.element(PRONUNCIATION_TAG));
    }

    public void setTiming(Element pronunciationToken, TimeFrame tf) {
        assert PRONUNCIATION_TAG.equals(pronunciationToken.getName());
        if (tf != null) {
            pronunciationToken.setAttribute(SPEECH_START_ATTR, Long.toString(tf.getStart()));
            pronunciationToken.setAttribute(SPEECH_END_ATTR, Long.toString(tf.getEnd()));
        }
    }

    /**
     * @param element The xml Node where the timing is attached
     * @return A TimeFrame with Start and End, or null if no timing is available.
     */
    public TimeFrame getTiming(Element element) {
        assert PRONUNCIATION_TAG.equals(element.getName()) || PHONE_TAG.equals(element.getName());
        String startAsString = element.getAttributeValue(SPEECH_START_ATTR);
        String endAsString = element.getAttributeValue(SPEECH_END_ATTR);
        if (startAsString != null && endAsString != null) {
            long start = Long.parseLong(startAsString);
            long end = Long.parseLong(endAsString);
            return new TimeFrame(start, end);
        } else {
            return null;
        }
    }
    //</editor-fold>

    /**
     * For a Pronunciation Token, returns if it has a timing.  For other nodes, such as Token, Sentence or Paragraph,
     * returns if all contained Pronunciation Tokens have timings.
     * If a node contains no Pronunciation Tokens at all, true is returned too!
     */
    public boolean hasCompleteTiming(Element node) {
        if (PRONUNCIATION_TAG.equals(node.getName()))
            return getTiming(node) != null;
        else
            return getDescendants(node, Filters.element(PRONUNCIATION_TAG), subtreeFilter)
                    .stream().allMatch(this::hasCompleteTiming);
    }

    public boolean hasSpokenParts(Element node) {
        if (PRONUNCIATION_TAG.equals(node.getName()))
            return true;
        else
            return !getDescendants(node, Filters.element(PRONUNCIATION_TAG), subtreeFilter).isEmpty();
    }

    private TimeFrame mergeTimeFrames(TimeFrame tf1, TimeFrame tf2) {
        if (tf1 == null)
            return tf2;
        if (tf2 == null)
            return tf1;
        long start = tf1.getStart() < tf2.getStart() ? tf1.getStart() : tf2.getStart();
        long end = tf1.getEnd() > tf2.getEnd() ? tf1.getEnd() : tf2.getEnd();
        return new TimeFrame(start, end);
    }

    /**
     * returns a timeframe with the earliest start time and latest end time found in any
     * normalized token in the subtree of the given element.
     * *Use with caution!*  If all normalized tokens in the subtree have timings, the resulting
     * TimeFrame is indeed the TimeFrame for the whole element.  If only the first and the last
     * normToken have timings, it might still be correct but it becomes a guess.  If tokens in the beginning
     * or in the end are missing timings, the start or end time are definitely inaccurate!
     * It can then only be used as the best guess.
     * @return Returns a TimeFrame as described above, or null if no timing at all is found.
     */
    public TimeFrame getAggregateTiming(Element node) {
        if (PRONUNCIATION_TAG.equals(node.getName())) {
            return getTiming(node);
        } else {
            return getDescendants(node, Filters.element(PRONUNCIATION_TAG), subtreeFilter).stream()
                    .reduce(null,
                            (tf, e) -> mergeTimeFrames(tf, getTiming(e)),
                            this::mergeTimeFrames);
        }
    }

    public Annotation toAnnotation(Element element) {
        assert PRONUNCIATION_TAG.equals(element.getName()) || PHONE_TAG.equals(element.getName());
        TimeFrame tf = getTiming(element);
        assert tf != null : "Cannot create Annotation without timing.";
        if (PRONUNCIATION_TAG.equals(element.getName())) {
        	return new SimpleAnnotation(tf, element.getAttributeValue(PRONUNCIATION_ATTR));
        } else {
        	return new SimpleAnnotation(tf, element.getAttributeValue(PHONE_TYPE_ATTR));
        }
    }

    /**
     * Takes the given node and wraps it in an 'ignore' tag.
     */
    public void ignoreNode(Element elem) {
        Element parent = elem.getParentElement();
        int index = parent.indexOf(elem);
        parent.removeContent(elem);
        Element ignored = new Element(IGNORE_TAG);
        ignored.addContent(elem);
        parent.addContent(index, ignored);
    }

    public void addPreambleTag(String text) {
        Element preamble = new Element(EXTRA_TAG);
        preamble.addContent(new Text(text));
        getBodyRoot().addContent(0, preamble);
    }

    public List<Element> getExtraTags() {
        return getNodes(Filters.element(EXTRA_TAG));
    }
    
	public void flattenIgnores() {
		flattenNestedTag(doc.getRootElement(), IGNORE_TAG);
	}
    
	public static void flattenNestedTag(Element e, String tag) {
		assert tag != null;
		flattenRecursively(e, e, tag, false);
	}

	/**  
	 * flatten nested tags so that just the top-level element of tag survives
	 * inIgnore indicates whether we've already within a "tag"-branch in which case further instances of tag should be dropped
	 *  - if a tag is to be dropped, we have to raise all its children up to the parent. that's done via the listIterator parentIt
	 *  - if a tag is OK, we recurse through all its children
	 *  
	 *  the trouble is that by dropping a tag, we change the parent's children (we add all tag's children in the appropriate place)
	 *  thus, when we recurse through the children of an element, we either take out too much (or too little) for non-obvious reasons
	 */
	private static void flattenRecursively(Element doc, Element e, String tag, boolean inIgnore) {
		if (tag.equals(e.getName())) {
			if (inIgnore) { // start removal
				assert e.getAttributes() == null
						|| e.getAttributes().isEmpty() : "You ask me to ignore a tag but it has attributes!";
				Element parent = e.getParentElement();
				assert parent != null : "something is wrong, I thought parent couldn't be null";
				List<Content> parentContent = parent.getContent();
				ListIterator<Content> parentCIt = parentContent.listIterator();
				// find the correct insertion point
				while (parentCIt.hasNext()) {
					if (parentCIt.next() == e) 
						break;
				}
				parentCIt.remove(); // this removes e (via the iterator)
				while (!e.getContent().isEmpty()) {
					Content c = e.getContent().get(0);
					c.detach();
					parentCIt.add(c);
				}
			} else {
				if (e.getContent().isEmpty()) {
					e.detach();
				}
			}
			inIgnore = true; // ignore once we have started to ignore
		}
		for (int i = 0; i < e.getChildren().size(); i++) { 
			// this list will actually change during the recursion which is why we must not iterate (but use a plain-old index-based for loop) plus some logic for dealing changes to the list
			Element child;
			do {
				child = e.getChildren().get(i); 
				flattenRecursively(doc, child, tag, inIgnore);
			} while (i < e.getChildren().size() && e.getChildren().get(i) != child); // repeat if the child has changed in the meantime
		}
	}

    /**
     * Progresses down the subtree given by the root element and takes the CDATA
     * contained in each element and adds it as a text attribute instead.
     * @param root  The root of the subtree.
     */
    public static void moveCdataToAttribute(Element root) {
        StringBuilder sb = new StringBuilder();
        root.getDescendants(Filters.text()).forEach(t -> sb.append(t.getText()));
        String text = sb.toString();
        if (!text.equals(""))
            root.setAttribute(TEXT_ATTR, sb.toString());
        root.removeContent(Filters.text());
        root.getChildren().forEach(SwcXmlUtil::moveCdataToAttribute);
    }


    //<editor-fold desc="Document query methods">
    /**
     * @param filter The filter for Content.
     * @return All nodes in the tree matching the filter, which are also in a subtree which is covered by the subtreeFilter.
     */
    private <F extends Content> List<F> getNodes(Filter<F> filter) {
        return getDescendants(getBodyRoot(), filter);
    }

    public <F extends Content> List<F> getDescendants(Element elem, Filter<F> filter) {
        return getDescendants(elem, filter, subtreeFilter);
    }

    /**
     * Like {@link Element#getDescendants(Filter)}, except that subtrees can be filtered out.  This was required to ignore
     * whole subtrees which are enclosed in an 'ignore' tag.
     * @param elem The Element which descendants should be looked at
     * @param filter The Filter to apply to elements which are candidates for the result list.
     * @param subtreeFilter The Filter to apply when deciding if the children of an element should be considered.
     * @return The descendants matching the filter and which are contained in a subtree whose root node has passed the subtree Filter.
     */
    private static <F extends Content> List<F> getDescendants(Element elem, Filter<F> filter, Filter<? extends Content> subtreeFilter) {
        List<F> results = new ArrayList<F>();
        collectDescendants(elem, filter, subtreeFilter, results);
        return results;
    }

    /**
     * This is a helper method for another method.
     * @see SwcXmlUtil#getDescendants(Element, Filter, Filter)
     */
    private static <F extends Content> void collectDescendants(Parent elem, Filter<F> filter, Filter<? extends Content> subtreeFilter, List<F> bag) {
        List<? extends Content> filteredContent = elem.getContent(subtreeFilter); // only descent into subtrees that match the subtreeFilter
        for (Content content : filteredContent) {
            F item = filter.filter(content); // only add items to the bag if they match the filter
            if (item != null)
                bag.add(item);
            if (content instanceof Parent)
                collectDescendants((Parent)content, filter, subtreeFilter, bag);
        }
    }
    //</editor-fold>

}
