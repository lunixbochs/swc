﻿using System.Xml;

namespace WikiDownloader
{
    //Don't forget to register your language in Downloader.Main in Downloader.cs
    class AudioDownloader_somelanguage : AudioDownloader
    {
        protected override string getLanguage () { return "unspecified"; }

        // used to analyze the page (XML) that accompanies each audio file and to extract meta data from there
        protected override ExtractedData ParseAudioPageXML(AudioMetadata audio_file, XmlDocument xmlDoc)
        {
            XmlElement template;
            if ((template = GetTemplateByTitle(xmlDoc, "TODO")) != null)
            {
                ExtractedData result = new ExtractedData { article_oldid = -1 };
                //TODO
                return result;
            }
            return new ExtractedData { article_oldid = -1 };
        }

        // used to find the templating information in the article text(XML) and fill in ** WHAT EXACTLY ** into ExtractedData
        protected override ExtractedData ParseArticleXML(long pageid, XmlDocument xmlDoc)
        {
            XmlElement template;
            if ((template = GetTemplateByTitle(xmlDoc, "TODO")) != null)
            {
                ExtractedData result = new ExtractedData { article_oldid = -1 };
                //TODO
                return result;
            }
            return new ExtractedData { article_oldid = -1 };
        }
    }
}
