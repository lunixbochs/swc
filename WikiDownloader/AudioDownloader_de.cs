﻿using System;
using System.Text.RegularExpressions;
using System.Xml;
using Util;

namespace WikiDownloader
{
    /// <summary>
    /// This class supports two ways of extracting audio information from German Wikipedia pages:
    /// - by the template "Gesprochener Artikel" or by the template "Gesprochene Version".	
    /// </summary>
    class AudioDownloader_de : AudioDownloader
    {

        protected override string getLanguage () { return "de"; }

        protected override ExtractedData ParseAudioPageXML(AudioMetadata audio_file, XmlDocument xmlDoc)
        {
            XmlElement template;
            if ((template = GetTemplateByTitle(xmlDoc, "Gesprochener Artikel")) != null)
            {
            	return ParseGesprochenerArtikelTemplate(template);
            }
            else
            {
				return base.ParseAudioPageXML (audio_file, xmlDoc);
            }
        }

        // parse the current article (XML-version) to find the recording information 
        protected override ExtractedData ParseArticleXML(long pageid, XmlDocument xmlDoc)
        {
            XmlElement template;
			ExtractedData result =  new ExtractedData { article_oldid = -1 };
            if ((template = GetTemplateByTitle(xmlDoc, "Gesprochener Artikel")) != null)
            {
				Log("Info", "Template found: Gesprochener Artikel");
				Log ("Debug", template.OuterXml);
                result = ParseGesprochenerArtikelTemplate(template);
            }
            else if ((template = GetTemplateByTitle(xmlDoc, "Gesprochene Version")) != null)
            {
				Log("Info", "Template found: Gesprochene Version");
				Log ("Debug", template.OuterXml);
                string[] filenames = null;
                foreach (var part in template.GetChildElements("part"))
                {
                    string key = part.GetChildElement("name").GetInnerTextOrNull();
                    string value = part.GetChildElement("value").GetInnerTextOrNull();
                    if (string.Equals(key, "datei", StringComparison.InvariantCultureIgnoreCase))
                    {
                        EnsureCapacity(ref filenames, 1);
                        filenames[0] = "Datei:" + value;
                    }
                    else if(Regex.IsMatch(key, "^datei[0-9]+$", RegexOptions.CultureInvariant))
                    {
                        int n = int.Parse(key.Substring("datei".Length));
                        EnsureCapacity(ref filenames, n);
                        filenames[n - 1] = "Datei:" + value;
                    }
                    else if (string.Equals(key, "version", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Match m;
                        if (value.TryMatch(@"\d{5,}", out m)) {
                            result.article_oldid = long.Parse(m.Value);
							result.oldid_quality = 2;
						}
                    }
                    else if (string.Equals(key, "datum", StringComparison.InvariantCultureIgnoreCase))
                    {
                        result.date_read = value;
                    }
                }
                result.audio_file_titles = filenames;
            }
            return result;
        }

        private static void EnsureCapacity<T>(ref T[] array, int n)
        {
            if (array == null)
                array = new T[n];
            else if (array.Length < n)
                Array.Resize(ref array, n);
        }

        // analyze contents of the template "Gesprochener Artikel"
        private ExtractedData ParseGesprochenerArtikelTemplate(XmlElement template)
        {
            ExtractedData result = new ExtractedData { article_oldid = -1 };
            string[] filenames = null;
            foreach (var part in template.GetChildElements("part"))
            {
                if (!part.HasChildElement("name"))
                    continue;

                string key = part.GetChildElement("name").InnerText.Trim().ToLowerInvariant();
                string value = part.GetChildElement("value").GetInnerTextOrNull();
                if (key == "dateiname")
                {
                    EnsureCapacity(ref filenames, 1);
                    filenames[0] = "Datei:" + value;
                } 
                else if (Regex.IsMatch (key, "^dateiname[0-9]+$", RegexOptions.CultureInvariant)) 
                {
                    int n = int.Parse(key.Substring("dateiname".Length));
                    EnsureCapacity(ref filenames, n);
                    filenames[n - 1] = "Datei:" + value;
                }
                else if (key == "sprecher")
                {
                    result.reader = value;
                }
                else if (key == "artikeldatum")
                {
                    result.date_read = value;
                }
                else if (key == "oldid")
                {
                    Match m;
                    if (value.TryMatch(@"\d{5,}", out m)) {
						Log("Info", "found oldid in Gesprochener Artikel Template");
                        result.article_oldid = long.Parse(m.Value);
						result.oldid_quality = 2;
					}
				}
            }
            result.audio_file_titles = filenames;
            return result;
        }
    }
}
