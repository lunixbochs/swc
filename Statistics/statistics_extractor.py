#! /usr/bin/env python3

# Copyright (c) 2015-2017, Arne Köhn <koehn@informatik.uni-hamburg.de>
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.


import copy
import inspect
import json
import math
import operator
import os
import statistics
import subprocess
from argparse import ArgumentParser
from lxml import etree as ET
import numpy as np
import functools

def parse_args():
    argp = ArgumentParser(description="Creates statistics for the given swc files")
    argp.add_argument("-o", "--outdir",
                      required = True,
                      help="directory to write the statistics to")
    argp.add_argument("-s", "--sources",
                      required = True,
                      help="Folder containing the article folders")
    argp.add_argument("-a", "--alignname",
                      default="align.swc",
                      help="filename for each alignment. Default: align.swc")
    return argp.parse_args()

def sentence_length(sentences):
    return sum([x.xpath("count(t)") for x in sentences])


def sentence_ms(sentence):
    res = sentence.xpath("(.//n)[last()]/@end - (.//n)[1]/@start")
#    if math.isnan(res):
#        ET.dump(sentence)
    return 0 if math.isnan(res) else res
    
def statistics_for_file(fname):
    res = {}
    data = ET.parse(fname)
    root = data.getroot()
    res["sentences"] = root.xpath("count(//s)")
    unaligned_sentences = root.xpath("//s[not(t/n/@start)]")
    aligned_sentences = root.xpath("//s[not(t/n[not(@start)])]")
    # some alternative code to check whether all n elements in a
    # sentence that are aligned are consecutive.  Turned out to be a
    # much harder criterion than having all n elements aligned.
    # all_sentences = root.xpath("//s")
    # consecutive_aligned_sentences = []
    # for s in all_sentences:
    #     # check if first and last element are aligned
    #     if s.xpath("count(.//n)") < 1:
    #         continue
    #     first = s.xpath(".//n[1]")[0]
    #     last = s.xpath(".//n[last()]")[-1]
    #     # check if first and last element are aligned
    #     if "start" in first.attrib and "end" in last.attrib:
    #         # check if alignments are consecutive
    #         consecutive = True
    #         t = first.attrib["end"]
    #         for elem in s.xpath(".//n[@start]")[1:]:
    #             if elem.attrib["start"] != t:
    #                 consecutive = False
    #                 break
    #             t = elem.attrib["end"]
    #         if consecutive:
    #             consecutive_aligned_sentences.append(s)
    res["unaligned_sentences"] =len(unaligned_sentences)
    res["aligned_sentences"] = len(aligned_sentences)
    res["unaligned_sentence_length_sum"] = sentence_length(unaligned_sentences)
    res["aligned_sentence_length_sum"] = sentence_length(aligned_sentences)

    # these are only fully aligned words!
    aligned_words = root.xpath('//t[n][not(n[not(@start)])]')
    aligned_normalized = sum([x.xpath('n') for x in aligned_words], [])
    
    res["aligned_words_sum"] = len(aligned_words)
    res["unaligned_words_sum"] = root.xpath('count(//t[n][not(n[@start])])')
    res["tokens"] = root.xpath('count(//t)')
    res["words"] = root.xpath('count(//t[n])')
    res["total_nonwhitespace_characters"] = sum([functools.reduce(lambda x,y: x if y in " \n" else x+1, x.text, 0) if not x.text is None else 0 for x in root.xpath('//t')])
    
    res['aligned_word_ms'] = sum([int(n.attrib['end']) - int(n.attrib['start']) \
                                  for n in aligned_normalized])
    # The hasattr is a workaround for words for which the normalization gets split and aligned, e.g.
    # 851 -> acht hundert einundfünfzig -> hundert and einundfünfzig will not hat an original word and
    # therefore no text attribute.  I consider this a bug in the alignment process.
    # This should be fixed in the journal data, but does not harm to keep it.
    res['aligned_word_forms'] = set([x.text.strip() for x in aligned_words if isinstance(x.text, str)])
    
    res['aligned_sentence_ms'] = sum([sentence_ms(x)
                                      for x in aligned_sentences])

    # phoneme stuff
    res['aligned_phonemes_ms'] = root.xpath("sum(//ph/@end)-sum(//ph/@start)")
    res['_phoneme_normalized_start_differences'] = [n.xpath("sum(./ph[1]/@start)") - int(n.attrib["start"]) for n in root.xpath("//n[ph and @start]")]
    res['_phoneme_normalized_end_differences'] = [n.xpath("sum(./ph[last()]/@end)") - int(n.attrib["end"]) for n in root.xpath("//n[ph and @end]")]
    return res

    
# num completely unaligned sentences
# align.getroot().xpath("/alignment/sentence[not(word[@start])]")

# completely aligned sentences
# align.getroot().xpath("/alignment/sentence[not(word[not(@start) and @normalized != ''])]")

# length of an aligned sentence in ms:
# _93[0].xpath("word[@stop][last()]/@stop - word[@start][1]/@start")

# anz worte
# _93[0].xpath("count(word)")

def sum_stats(stats):
    """add single stat to overall stats"""
    res = None
    for stat in stats:
        if res == None:
            res = copy.deepcopy(stat)
            try:
                res.pop('speaker')
            except:
                pass
        else:
            for key in res:
                if key == 'aligned_word_forms':
                    res[key] = res[key].union(stat[key])
                else:
                    res[key] = res[key] + stat[key]
    return res


class allkindsofstats:
    def __init__(self, stats, article_stats, args):
        self.stats = stats
        self.article_stats = article_stats
        print([x[0] for x in inspect.getmembers(self)])
        for name, func in inspect.getmembers(self, inspect.ismethod):
            print(name)
            if name.startswith("_"):
                continue
            print("\nrunning "+name+":\n")
            res = func()
            print(res)
            with open(args.outdir + '/' + name+'.txt', 'w') as f:
                f.write(res)

    def general_stats(self):
        res = []
        for k,v in self.stats.items():
            if k.startswith("_"):
                continue
            if k == 'aligned_word_forms':
                res.append(k+"\t"+str(len(v)))
            else:
                res.append(k+"\t"+str(v))
                if k.endswith("ms"):
                    res.append(k[:-2]+"h\t"+str(v/1000.0/60/60))
        res.sort()
        return '\n'.join(res)

    def inspect_errors(self):
        res = ''
        for article, stat in self.article_stats.items():
            if stat['audio_ms'] < stat['aligned_sentence_ms']:
                res += article + ' ' + str(stat['audio_ms']) + ' ' + str(stat['aligned_sentence_ms'])+ '\n'
        return res

    @staticmethod
    def _list_mean_median_etc(data):
        s_data = sorted(data)
        res = "avg: " + str(statistics.mean(s_data))
        res += "\nmedian: " + str(statistics.median(s_data))
        res += "\n5%: " + str(s_data[int(len(s_data)*0.05)])
        res += "\n25%: " + str(s_data[int(len(s_data)*0.25)])
        res += "\n75%: " + str(s_data[int(len(s_data)*0.75)])
        res += "\n95%: " + str(s_data[int(len(s_data)*0.95)])
        return res

    def phoneme_stats(self):
        res = ""
        for pos in ["start", "end"]:
            data = self.stats['_phoneme_normalized_'+pos+'_differences']
            res += pos + " differences:\n"
            res += "------------------\n"
            res += self._list_mean_median_etc(data) + "\n"
            res += "root mean squared error: " + str(np.sqrt(np.mean(np.array(data)**2))) + "\n"
            res += "absolute mean error: " + str(np.mean(np.abs(np.array(data)))) + "\n\n"
        return res
            


        

    def author_stats(self):
        author_articles = {}
        author_stats = {}
        for stat in self.article_stats.values():
            if not stat['speaker'] in author_articles:
                author_articles[stat['speaker']] = []
            author_articles[stat['speaker']].append(stat)
        recognized_words_ms = []
        recognized_sentences_ms = []
        recognized_sentences_ratio = []
        audio_ms = []
        
        for author, stats in author_articles.items():
            author_stats[author] = sum_stats(stats)
            recognized_words_ms.append(author_stats[author]['aligned_word_ms'])
            recognized_sentences_ms.append((author_stats[author]['aligned_sentence_ms'], author))
            audio_ms.append(author_stats[author]['audio_ms'])
            ratio = author_stats[author]['aligned_sentence_ms']/author_stats[author]['audio_ms']
            if (ratio > 1):
                print("author has a ratio >1")
                print(author)
            recognized_sentences_ratio.append(ratio)
        audio_recognized_tuples = list(zip(audio_ms, recognized_words_ms))
        audio_recognized_tuples.sort()
        recognized_words_ms.sort()
        #recognized_sentences_ms.sort()
        #recognized_sentences_ratio.sort()
        audio_ms.sort()
        with open(args.outdir + '/' +"recognized_words_ms_author", 'w') as f:
            f.write("\n".join([str(x) for x in  recognized_words_ms]))
        #with open("recognized_sentences_ms_author", 'w') as f:
        #    f.write("\n".join([str(x) for x in  recognized_sentences_ms]))
        #with open("recognized_sentences_ms_ratio", 'w') as f:
        #    f.write("\n".join([str(x) for x in  recognized_sentences_ratio]))
        with open(args.outdir + '/' + "audio_and_recognized_words_minutes", 'w') as f:
            f.write("\n".join(['%f %f' %(x[0]/1000.0/60, x[1]/1000.0/60) for x in audio_recognized_tuples]))
        res = "recognized_words_ms_author stats:\n"
        res += self._list_mean_median_etc(recognized_words_ms) + "\n\n"
        # res += "recognized_sentences_ms_author stats:\n"
        # res += self._list_mean_median_etc([x[0] for x in recognized_sentences_ms]) + "\n\n"
        # res += "recognized_sentences_ratio stats:\n"
        # res += self._list_mean_median_etc(recognized_sentences_ratio) + "\n\n"
        res += "audio_ms stats:\n"
        res += self._list_mean_median_etc(audio_ms) + "\n\n"
        return res

                

def get_audio_length_for_article(article, sources_dir):
    audio_file = sources_dir + "/" + article + "/audio.wav"
    # for some reason, everything is escaped except sometimes . so do it by ourselves
    if not os.path.exists(audio_file):
        audio_file = sources_dir + "/" + article.replace(".", "%2e") + "/audio.wav"
    return float(subprocess.check_output(["soxi", "-D", audio_file]))*1000


def get_speaker_for_article(article, sources_dir):
    info_json_file = sources_dir + "/" + article + "/info.json"
    if not os.path.exists(info_json_file):
        # for some reason, everything is escaped except sometimes . so do it by ourselves
        info_json_file = sources_dir + "/" + article.replace(".", "%2e") + "/info.json"
    speaker = None
    try:
        with open(info_json_file) as f:
            info_json = json.load(f)
            if "error" in info_json and info_json["error"] != None:
                #print("error in "+article)
                #print(info_json)
                return "ERROR"
            try:
                speaker = info_json['audio_file_parsed']['reader']
            except:
                speaker = info_json['audio_file']['reader']
    except Exception as e:
        print(e)
        print("Error in article "+article)
        speaker = "ERROR"
    return speaker


if __name__ == "__main__":
    single_stats = {}
    stats = None
    args = parse_args()
    try:
        os.mkdir(args.outdir)
    except:
        pass
    for article in os.listdir(args.sources):
        print("running", article)
        try:
            fname = args.sources+"/"+article+'/'+args.alignname
            if not os.path.exists(fname):
                print('skipping ' + article + ' because no ' + args.alignname + ' present')
                continue
            file_stats = statistics_for_file(fname)
            # extract article name from /path/to/article.xml and store stats
            # article = os.path.splitext(os.path.basename(in_file))[0]
            speaker = get_speaker_for_article(article, args.sources)
            if speaker == "ERROR":
                continue
            try:
                file_stats["audio_ms"] = get_audio_length_for_article(article, args.sources)
            except:
                continue
            single_stats[article] = file_stats
            # add later because it should not go into stats
            file_stats['speaker'] = speaker
        except IOError as e:
            print(e)
            exit(1)
    stats = sum_stats(single_stats.values())
    allkindsofstats(stats, single_stats, args)
