﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Util
{
    public static class Json
    {
		/*
        [ThreadStatic]
        static JavaScriptSerializer _serializer;

        static JavaScriptSerializer Serializer
        {
            get
            {
                var result = _serializer;
                if(result == null)
                {
                    _serializer = result = new JavaScriptSerializer();
                }
                return result;
            }
        }

        public static ExpandoObject Decode(string json)
        {
            IDictionary<string, object> dictionary;
            try
            {
                dictionary = Serializer.Deserialize<IDictionary<string, object>>(json);
            }
            catch(Exception ex)
            {
                throw new Exception("Can't parse json: "+json,ex);
            }
            return dictionary.Expando();
        }

        public static T Decode<T>(string json)
        {
            return Serializer.Deserialize<T>(json);
        }
       
        
        public static ExpandoObject Expando(this IDictionary<string, object> dictionary)
        {
            ExpandoObject expandoObject = new ExpandoObject();
            IDictionary<string, object> objects = expandoObject;

            foreach (var item in dictionary)
            {
                bool processed = false;

                if (item.Value is IDictionary<string, object>)
                {
                    objects.Add(item.Key, Expando((IDictionary<string, object>)item.Value));
                    processed = true;
                }
                else if (item.Value is ICollection)
                {
                    List<object> itemList = new List<object>();

                    foreach (var item2 in (ICollection)item.Value)

                        if (item2 is IDictionary<string, object>)
                            itemList.Add(Expando((IDictionary<string, object>)item2));
                        else
                            itemList.Add(Expando(new Dictionary<string, object> { { "Unknown", item2 } }));

                    if (itemList.Count > 0)
                    {
                        objects.Add(item.Key, itemList);
                        processed = true;
                    }
                }

                if (!processed)
                    objects.Add(item);
            }

            return expandoObject;
        }

        public static string Encode(object @object)
        {
           return Serializer.Serialize(@object);
        }//*/

        public static object Decode(string json)
        {
            return MakeDynamic(JsonConvert.DeserializeObject(json));

        }

        private static object MakeDynamic(object v)
        {
            if (v == null)
                return null;
            if(v is JArray)
            {
                JArray jarray = (JArray)v;
                return jarray.Select(MakeDynamic).ToList();
            }
            if (v is JObject)
            {
                JObject jobject = (JObject)v;
                ExpandoObject result = new ExpandoObject();
                IDictionary<string, object> dict = result;
                foreach (JProperty prop in jobject.Properties())
                {
                    dict[prop.Name] = MakeDynamic(prop.Value);
                }
                return result;
            }
            if (v is JValue)
            {
                JValue value = (JValue)v;
                if (value.Type == JTokenType.String)
                    return (string)value.Value;
                if (value.Type == JTokenType.Date)
                    return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
            }
            return v;
        }

        public static T Decode<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string Encode(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }//*/

    }
}
