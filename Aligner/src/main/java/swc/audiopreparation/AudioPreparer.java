package swc.audiopreparation;

import ie.corballis.sox.SoXEffect;
import ie.corballis.sox.SoXEncoding;
import ie.corballis.sox.Sox;
import ie.corballis.sox.WrongParametersException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Created by felix on 13/02/17.
 */
public class AudioPreparer {

    private final File articleDir;
    private final Sox sox;

    public AudioPreparer(File articleDir) {
        if (!articleDir.isDirectory() || !articleDir.exists())
            throw new IllegalArgumentException("The directory does not exist or is not a directory");
        this.articleDir = articleDir;
        sox = new Sox("/usr/bin/sox");
    }

    private List<String> getInputFiles() {
        File[] files = articleDir.listFiles((dir, fn) -> fn.startsWith("audio") && fn.endsWith(".ogg"));
        List<String> result = Arrays.stream(files)
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
        Collections.sort(result);
        return result;
    }

    private String getOutputFilename() {
        return new File(articleDir, "audio.wav").getAbsolutePath();
    }

    private List<String> convertToWav(List<String> inputFiles) throws Exception {
        List<String> newFiles = new ArrayList<>();
        for (String inputFile : inputFiles) {
            String outFilename = inputFile + ".wav";
            try {
                sox.inputFile(inputFile)
                        .bits(16)
                        .argument("--endian", "little")
                        .argument("--channels", "1")
                        .encoding(SoXEncoding.SIGNED_INTEGER)
                        .argument("--rate", "16000")
                        .outputFile(outFilename)
                        .effect(SoXEffect.HIGHPASS, "10")
                        .execute();
                newFiles.add(outFilename);
            } catch (IOException|WrongParametersException e) {
                throw new Exception("Error with sox", e);
            }
        }
        return newFiles;
    }

    private void mergeFiles(List<String> inputFiles, String outputFile) throws Exception {
        Sox s = sox;
        for (String iFile : inputFiles) {
            s = s.inputFile(iFile);
        }
        try {
            s.outputFile(outputFile)
                    .execute();
        } catch (IOException|WrongParametersException e) {
            throw new Exception("Error with sox", e);
        }
    }

    private void removeIntermediateFiles(List<String> filesToRemove) {
        for (String s : filesToRemove) {
            new File(s).delete();
        }
    }

    public void doStuff() throws Exception {  // TODO logging
        List<String> inputFiles = getInputFiles();
        List<String> intermediateFiles = convertToWav(inputFiles);
        mergeFiles(intermediateFiles, getOutputFilename());
        removeIntermediateFiles(intermediateFiles);
    }
}
