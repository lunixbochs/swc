#!/usr/bin/perl -I /informatik2/nats/home/baumann/perl5/lib/perl5
use strict;
use warnings;
use JSON;
use Audio::Wav;
use Audio::Wav::Read;
use List::Util qw(sum);

# gets info.json files as cmdline arguments and parses these 
# to determine the speaker. Then, for all audios read by the 
# same speaker, calls audio-duration.pl to determine the speaker's
# audio

my %speakerAliases; # key: alias, value: actual speaker name to be used
if ($#ARGV > 1) {
	if ($ARGV[0] eq '-a') {
		open ALIASES, '<', $ARGV[1];
		shift @ARGV;
		shift @ARGV;
		while (my $line = <ALIASES>) {
			next if ($line =~ m/^\s*#/); # ignore comment lines
			chomp $line; chomp $line;
			my ($actual, @aliases) = split "\t", $line;
			foreach my $alias (@aliases) {
print STDERR "adding alias $alias for $actual\n";
				$speakerAliases{$alias} = $actual;
			}
		}
		close ALIASES;
	}
}

my @files = @ARGV;

my %readers; # speakers as keys, list of audio durations as values
my %readerSums;

print "#file,reader,duration,aligned,propAligned,words,alWords,propWords\n";
foreach my $file (@files) {
print "$file\t";
	my $reader = getReader($file);
	my $duration = getDuration($file);
	my ($alignedTime, $alignedWords, $totalWords) = getAligned($file);
	print "$reader\t$duration\t$alignedTime\t" 
		. ($duration != 0 ? sprintf("%.3f", $alignedTime / $duration) : "") 
		. "\t$totalWords\t$alignedWords\t"
		. ($totalWords != 0 ? sprintf("%.3f", $alignedWords / $totalWords) : "")
		. "\n";
	#print "progress: $duration seconds of audio for reader $reader in file $file\n";
	push @{$readers{$reader}}, $duration;
	$readerSums{$reader} += $duration;
}

my $totalSum;
my $i;
foreach my $reader (sort { $readerSums{$a} <=> $readerSums{$b} } keys %readerSums) {
	print "$reader: " . $readerSums{$reader} . "\t" . scalar @{$readers{$reader}} . " \n";
	$totalSum += $readerSums{$reader};
	print ++$i . " " . $totalSum . " " . $readerSums{$reader} . "\n";
}

sub getReader {
	my $file = shift @_;
	open JSON, '<', $file or die "Could not open JSON file $file";
	my $json = from_json((join " ", <JSON>), { utf8 => 1 });
	close JSON;
	my $reader = $json->{audio_file_parsed}->{reader} or $json->{article_parsed}->{reader};
	$reader = $speakerAliases{$reader} if (exists $speakerAliases{$reader});
	$reader =~ s/ /_/g;
	return $reader;
}	

sub getDuration {
	my $file = shift @_;
#	$file =~ s/info.json/audio.wav/ or warn "was expecting filename to be info.json, but it's not." and return 0;
#	my $wav = new Audio::Wav;
#	my $read;
#	eval {
#	$read = $wav->read($file) } or warn "could not open WAV file $file." and return 0;
#	my $duration = $read->length_seconds();
#	$read->{handle}->close();
#	$read = undef;
	$file =~ s/info.json/audio.wav/ or warn "was expecting filename to be info.json, but it's not." and return 0;
	#$file =~ s/info.json/audio-duration/ or warn "was expecting filename to be info.json, but it's not." and return 0;
	my $duration = `soxi -D "$file"`; 
	#my $duration = `cat "$file"`;
	chomp $duration; chomp $duration;
	return $duration;
}

sub getAligned {
	my $alignmentPath = "../results/";
	my $file = shift @_;
	$file = $alignmentPath . $file;
	$file =~ s"info.json"temp/aligned";
	open ALIGNED, '<', $file or warn "could not open $file, skipping." and return 0;
	my $alignedMS = 0;
	my $totalWords = 0;
	my $alignedWords = 0;
	while (my $line = <ALIGNED>) {
		$totalWords++;
		$line =~ m/\[(\d+)\:(\d+)\]$/ or next;
		$alignedWords++;
		$alignedMS += $2 - $1;
	}
	close ALIGNED;
	return ($alignedMS / 1000, $alignedWords, $totalWords);
}
