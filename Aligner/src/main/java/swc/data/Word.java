package swc.data;

import java.util.List;

public class Word {
	public String original;
	public List<Token> parts;

	public boolean isAligned() {
		for (Token t : parts) {
			if (! t.isAligned())
				return false;
		}
		return true;
	}
}
