﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Util;

namespace WikiDownloader
{
    class TextDownloader : Downloader
    {
        public static void Run(Configuration config)
        {
			IList<string> pageTitles;
			if (config.category == null || config.category.Equals("")) {
				pageTitles = config.additionalArgs;
				if (pageTitles == null || pageTitles.Count == 0) {
					pageTitles = GetAllArticlePages (config.apiURL, true).Select (v => v.Value).ToArray ();
				}
			} else {
				pageTitles = GetPagesInCategory(config.apiURL, config.category, true).Select(v => v.Value).ToArray();
			}
            Log("info", "Number of page titles is: " + pageTitles.Count);
			Log ("debug", pageTitles.ToString ());
            foreach(var title in pageTitles)
            {
				//Create a directory for the page, if it does not already exist
				string dir = Path.Combine(config.outputDirectory, EncodeFileName(title));
				Directory.CreateDirectory(dir);
				if (config.format == null || config.format.Equals ("") || config.format.Equals ("all")) {
					string fileprefix = Path.Combine(config.outputDirectory, Path.Combine(EncodeFileName(title), "current."));
					DownloadFile (config.apiURL, title, "html", fileprefix + "html");
					DownloadFile (config.apiURL, title, "xml", fileprefix + "xml");
					DownloadFile (config.apiURL, title, "txt", fileprefix + "txt");
				} else {
					string filename = Path.Combine(config.outputDirectory, Path.Combine(EncodeFileName(title), "current." + config.format));
					DownloadFile (config.apiURL, title, config.format, filename);
                }
            }
        }

		public static List<KeyValuePair<long, string>> GetAllArticlePages(string api, bool retrieveTitles = false) {
			string query = api 
				+ "action=query"
				+ "&list=allpages"
				+ "&apto=B"
				+ "&apminsize=1024" // ignore overly short articles
				+ "apfilterredir=nonredirects" // no need to download redirects
				+ "&format=json";
			return GetPagesViaAPI (query, "ap", "allpages", retrieveTitles);
		}

		private static void DownloadFile(string api, string title, string format, string filename)
        {
            string propertyname; 
            
            Log("info", "Downloading " + title);
            string url = api
                + "action=parse"
                + "&format=json"
                + "&page=" + Uri.EscapeDataString(title)
                + "&redirects="
                + "&prop=";

            switch(format)
            {
                case "html": url += "text"; propertyname = "text"; break;
                case "txt": url += "wikitext"; propertyname = "wikitext"; break;
                case "xml": url += "properties&generatexml="; propertyname = "parsetree"; break;
                default: throw new ArgumentException("invalid file type");
            }
            
            string json_text = DownloadTextFile(url);
            dynamic data = Json.Decode(json_text);
            if (HasProperty(data, "error"))
            {
                dynamic error = data.error;
                string code = (string)error.code;
                if (code == "missingtitle")
                    Log("ERROR", "Article not found: " + title);
                else
                {
                    string info = HasProperty(error, "info") ? (string)error.info : null;
                    throw new Exception("Api error: " + code + " " + info);
                }
            }
            else
            {
                dynamic prop = GetProperty(data.parse, propertyname);
                string text = (string)GetProperty(prop, "*");
                File.WriteAllText(filename, text);
            }
        }
    }
}
