﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Util;

namespace WikiDownloader
{
    public class Downloader
    {
        [Serializable]
        public class Configuration
        {
            public string apiURL { get; set; }
            public string indexURL { get; set; }
            public string category { get; set; }
            public string outputDirectory { get; set; }
            public string retrieve { get; set; }
            public string format { get; set; }
			public Boolean skipAudio { get; set; }
            public IList<string> additionalArgs { get; set; }
        }

        static JobQueue tasks;
        // For more info on the api:
        // https://www.mediawiki.org/wiki/API:Main_page
        // https://en.wikipedia.org/wiki/Special:ApiSandbox

        static void Main(string[] args) {
            //string api = "http://en.wikipedia.org/w/api.php?";
            //string category = "Category:Spoken_articles";
            //string outputDirectory = "output";

            string configFileName = "config.json";
			if (args.Length == 0) {
				Console.WriteLine ("Usage: WikiDownloader <language>.json <target_base_dir> [list of articles]");
			} else {
				if (args.Length > 0) {
					configFileName = args [0];
				}
				var config = Json.Decode<Configuration> (File.ReadAllText (configFileName));
				// a list of articles that should be included in operation, given as additional arguments
				if (args.Length > 1) {
					config.outputDirectory = args [1];
					config.additionalArgs = args.Skip (2).ToList ();
					if (!config.additionalArgs.Any ()) // make null if there's nothing inside
                    config.additionalArgs = null;
				}

				tasks = new JobQueue (4);
				ServicePointManager.DefaultConnectionLimit = 4;
				Dictionary<string,Action<Configuration>> retrieve = new Dictionary<string,Action<Configuration>> ();
				retrieve ["spoken_en"] = new AudioDownloader_en ().Run;
				retrieve ["spoken_de"] = new AudioDownloader_de ().Run;
				retrieve ["spoken_nl"] = new AudioDownloader_nl ().Run;
				retrieve ["text"] = TextDownloader.Run;
				if (!retrieve.ContainsKey (config.retrieve)) {
					Console.Error.WriteLine ("config.retrieve must have one of the following values: " + string.Join (" ", retrieve.Keys));
					Environment.Exit (-1);
				}
				try {
					retrieve [config.retrieve] (config);
				} finally {
					tasks.Close ();
				}
			}
        }

        public static List<KeyValuePair<long, string>> GetPagesInCategory(string api, string category, bool retrieveTitles = false) {
            string query = api
                         + "action=query"
                         + "&list=categorymembers"
                         + "&format=json"
                         + "&cmtitle=" + Uri.EscapeDataString (category)
                         + "&cmprop=" + Uri.EscapeUriString (retrieveTitles ? "ids|title" : "ids")
                         + "&cmtype=page";
            return GetPagesViaAPI(query, "cm", "categorymembers", retrieveTitles);
        }

		protected static List<KeyValuePair<long, string>> GetPagesViaAPI(string query, string paramtype, string fieldname, bool retrieveTitles = false) {
			var result = new List<KeyValuePair<long, string>>();
			string continueMarker = null;
			int i = 0;
			do {
				string url = query 
					+ "&" + paramtype + "continue=" + continueMarker
					+ "&" + paramtype + "limit=max";
				//Log("Debug", url);
				string json = DownloadTextFile(url);
				//Log("info", json);
				dynamic data = Json.Decode(json);
				try {
					dynamic continuation = GetProperty(data, "continue");
					continueMarker = (string)GetProperty(continuation, paramtype +"continue");
				} catch {
					continueMarker = null;
				}
				foreach (dynamic member in GetProperty(data.query, fieldname)) {
					long id = (long)member.pageid;
					string title = null;
					if (retrieveTitles) {
						title = (string)member.title;
					}
					result.Add(new KeyValuePair<long, string>(id, title));
				}
				Console.Out.Write(".");
				i++;
				if (i % 100 == 0) {
					Console.Out.Write("\nindexed " + result.Count + "\n");
				}
			}
			while (!string.IsNullOrEmpty(continueMarker));
			return result;
		}

        public static dynamic HasProperty(dynamic @object, string key) {
            return ((IDictionary<string, object>)@object).ContainsKey(key);
        }

        public static dynamic GetProperty(dynamic @object, string key) {
            return ((IDictionary<string, object>)@object)[key];
        }

        /// <summary>
        /// Encodes the string using utf8 first and url encoding afterwards.
        /// The encoded string contains only ascii characters and does not start with a dot ('.').
        /// To decode: First url decode, then utf8 decode.
        /// </summary>
        /// <param name="name">The file name to encode</param>
        /// <returns>The encoded string</returns>
        public static string EncodeFileName(string name)
        {
            var bytes = Encoding.UTF8.GetBytes(name);
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                var c = bytes[i];
                if (c == ' ')
                    result.Append('_');
                else if ((c >= 'A' && c <= 'Z') ||
                    (c >= 'a' && c <= 'z') ||
                    (c >= '0' && c <= '9') ||
                    (i > 0 && i<bytes.Length-1 && c == '.') ||//File names must not start (linux) or end (windows) with dots
                    "+-_#{}()&$§! ".Contains((char)c))
                {
                    result.Append((char)c);
                }
                else
                {
                    result.Append('%');
                    result.Append(Convert.ToString(c,16).PadLeft(2,'0'));
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Downloads/Opens and decodes a text file in memory
        /// </summary>
        /// <param name="url">The url of the decoded file</param>
        /// <returns>The decoded text</returns>
        public static string DownloadTextFile(string url)
        {
            for (int tries_left = 3; ; tries_left--)
            {
                try
                {
                    var request = WebRequest.Create(url);
                    using (var response = request.GetResponse())
                    {
                        string charset = null;
                        var httpResponse = response as HttpWebResponse;
                        if (httpResponse != null)
                        {
                            if (httpResponse.StatusCode != HttpStatusCode.OK)
                            {
                                throw new WebException("Status code was: " + httpResponse.StatusCode);
                            }
                            charset = httpResponse.CharacterSet;
                        }
                        Encoding enc = charset != null ? Encoding.GetEncoding(charset) : null;
                        using (var reader = new StreamReader(GetResponseStreamChecked(response), enc, true))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
                catch(WebException)
                {
                    if (tries_left <= 0)
                        throw;
                }
            }
        }

        public static void DownloadFile(string url, string filename)
        {
            for (int tries_left = 3; tries_left > 0; tries_left--)
            {
                try
                {
                    var request = WebRequest.Create(url);
                    using (var response = request.GetResponse())
                    using (var stream = GetResponseStreamChecked(response))
                    using (var file = File.OpenWrite(filename))
                    {

                        file.SetLength(0);
                        var buffer = new byte[0x10000];
                        int n;
                        while ((n = stream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            file.Write(buffer, 0, n);
                        }
                    }
                    return;//success
                }
                catch(WebException e)
                {
                    if(tries_left == 0)
                    {
                        Log("ERROR", "Failed to download " + url + " because "+e.Message);
                    }
                }
            }
        }

        private static Stream GetResponseStreamChecked(WebResponse response)
        {
            HttpWebResponse httpResponse = response as HttpWebResponse;
            if(httpResponse != null)
            {
                long contentLength = httpResponse.ContentLength;
                if(contentLength > 0 &&
                    string.IsNullOrEmpty(httpResponse.Headers.Get("Transfer-Encoding")) &&
                    string.IsNullOrEmpty(httpResponse.ContentEncoding))
                {
                    return new StreamSlice(response.GetResponseStream(), contentLength, true);
                }
            }
            return response.GetResponseStream();
        }

        public static void Log(string category, string text)
        {
            Console.WriteLine(("["+category+"]").PadRight(8) + text);
        }
    }
}
