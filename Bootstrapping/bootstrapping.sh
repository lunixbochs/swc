#! /bin/bash

set -eu
# -e exits on non-zero return values of simple commands
# -u prevents access to non-existent variables

# Requirements (to do before starting the script):
#     - master_script.sh: run installation step -> all software installed (Aligner.jar especially)
#     - Execute: install_sequitur.sh, install_sphinxtrain.sh
#     - In $PATH there should be: python3, sphinxtrain
#     - Working directory should look like:
#       .
#         <this_file>
#         sequitur/
#           apply_model.sh
#         sphinxtrain/   <- installation directory for sphinxtrain
#           SOURCEME
#         $FILEGEN_SCRIPT (filegen_sphinxtrain.py by default, file should be there as is)
#     - otherwise: update paths below

MASTER_SCRIPT="../../master_script.sh"
ALIGNER_JAR="../Aligner/target/Aligner.jar"
FILEGEN_SCRIPT="filegen_sphinxtrain.py"

# Article preparation:
#     - articles need to be downloaded, preparation needs to be done beforehand
#     - articles should have: audio.txt, audio.wav.  otherwise article is ignored

# Potential Hickups:
#     - master_script needs to know the code directory, if its not the default location
#     - -p (number of processes) and -r (amount of ram) are not set here. 

# How it works:
#     The script uses flags do determine where it is in the iteration process.
#     It can be stopped, a step can be completed manualle, i.e. aligning more articles,
#     and then the script can be started again and it will detect the progress an continue with
#     the next step



# flag mechanism to pick up the process when it is disturbed
# set flags to mark steps as finished
#   use a directory with files as flags
#   step granularity as it_00_align, it_00_snippets, it_00_model, it_01_align, ...


# intended directory structure:
# run_directory/
#   articles/
#   (initial_model/)
#   iterations/
#     iteration00/
#       flags/               # contains files as flags to mark completed steps
#       alignment_results/   # created with model passed via cmd line
#       wav/                 # created from alignment_results, called wav b/c of sphinxtrain
#       new_model/           # trained from snippets
#       ...                  # other files created by sphinxtrain
#     iteration01/
#       alignment_results/   # with model from prev. iteration-step

# iteration step function:
#   receives a model and an article directory
#   creates a new model
#   substeps:
#     align articles with given model
#     create snippets from alignments
#     train new model

allAligned() {
    # args:
    articles_dir=$1
    alignment_file_name=$2

    ls -1 "$articles_dir" | \
        while read dir; do
            if [ -f "$articles_dir/$dir/audio.wav" -a -f "$articles_dir/$dir/audio.txt" \
                 -a ! -f "$articles_dir/$dir/$alignment_file_name" ]; then
                return 1
            fi
        done
    return 0
}


language=german  #german|english|dutch
ffmpeg=ffmpeg
G2P_model=""         # must be given via switches. stays the same throughout iterations
G2P_model_CAPS=false #true|false
model=""
iterationsdir="iterations"
articlesdir="articles"


iterationStep() {
    # args:
    target_dir=$1    # absolute path where all the stuff from one iteration goes
    articles_dir=$2  # directory containing the articles
    model_dir=$3     # directory of model to use of alignment
    itname=$4        # something like it01, will be used for various identifiers in filenames etc.
    alignment_file_name=$4.swc # file name for the alignment file for this iteration e.g: it01.swc
    # creates:
    #     a new model to use for the next iteration
    # steps:
    #     1. align stuff
    #     2. generate snippets for training
    #     3. generate other stuff for training
    #     4. train new model (sphinxtrain)

    # 1. align
    if [ -f $target_dir/flags/alignment_done ]; then
        echo "In $target_dir alignment is done. Continuing."
    else
        echo "Starting Align"
        # define job directory
        a_job_dir=$target_dir/align_jobs

        # if no job directory exists, generate jobs
        if [ ! -d $a_job_dir ]; then
            echo "generating align jobs"
            # -g, -d implied though -m  (-r for amount of ram not set)
            bash $MASTER_SCRIPT --gen-align-jobs -i .. -l $language \
                 -a $articles_dir -j $a_job_dir \
                 -m $model_dir -o $alignment_file_name
        fi

        # execute jobs
        echo "executing align jobs"
        bash $MASTER_SCRIPT -E -i .. -l $language -j $a_job_dir # (-p for amount of processes not set)

        # check if all is aligned
        allAligned $articles_dir $alignment_file_name
        local aligned=$?
        if [ $aligned -eq 0 ]; then
            mkdir -p $target_dir/flags && touch $target_dir/flags/alignment_done
        else
            echo "Not everything aligned." && exit 1
        fi
    fi

    # 2. generate snippets for training
    if [ -f $target_dir/flags/snippets_done ]; then
        echo "In $target_dir snippets is done. Continuing."
    else
        echo "Generating snippets"
        snippets_dir=$target_dir/wav   # needs to be called 'wav' for sphinxtrain
        mkdir -p $snippets_dir
        ls -1 "$articles_dir" | \
            while read dir; do
                if [ ! -e "$articles_dir/$dir/$alignment_file_name" ]; then
                    continue # no alignment file. skip.
                fi
                if [ ! -e "$snippets_dir/$dir" -o \
                       -e "$snippets_dir/$dir" -a ! -e "$snippets_dir/$dir/done" ]; then
                    # directory does not exist or exists but not done
                    rm -r "$snippets_dir/$dir" && mkdir -p "$snippets_dir/$dir"
                    echo "Generating snippets for $dir"
                    timeout 30m java -jar $ALIGNER_JAR extractsnippets training \
                            "$articles_dir/$dir/audio.wav" \
                            "$articles_dir/$dir/$alignment_file_name" \
                            "$snippets_dir/$dir"
                    rt=$?
                    if [ $rt -eq 124 ]; then
                        echo "Generating snippets for $dir - timeout" && exit 2
                    else
                        touch "$snippets_dir/$dir/done"
                    fi
                else
                    echo "Generating snippets for $dir - skipped"  # snippets dir exists already
                fi
            done
        # iterated through everything and everything is done or script exited
        mkdir -p $target_dir/flags && touch $target_dir/flags/snippets_done
    fi

    # 3. generate other stuff for training (uses the python helper)
    if [ -e $target_dir/flags/model_done ]; then
        echo "model already generated"
    else
        # generate a bunch of files
        caps=""
        [ G2P_model_CAPS = "true" ] && caps="--caps"
        python3 $FILEGEN_SCRIPT gen --prefix $target_dir --no_test $caps \
                sequitur/apply_model.sh $G2P_model $itname
        
        # source env vars
        . sphinxtrain/SOURCEME 

        # sphinxtrain setup, generates other files
        cd $target_dir
        sphinxtrain -t $itname setup
        cd -

        # modify the config file for our usecase
        python3 $FILEGEN_SCRIPT cfg $target_dir/etc/sphinx_train.cfg

        # build the model
        cd $target_dir
        sphinxtrain --stages comp_feat,verify,lda_train,mllt_train,ci_hmm,cd_hmm_untied,buildtrees,prunetree,cd_hmm_tied run
        cd -
        if [ -e $target_dir/means ]; then
            mkdir -p $target_dir/flags && touch $target_dir/flags/model_done
        else
            exit 1
        fi
    fi
}




bootstrap() {
    # args:
    iterations_dir=$1   # directory where to put the iterations
    articles_dir=$2     # directory containing the articles
    initial_model=$3    # initial model directory, used for initial alignment
    n=$4                # number of iterations to make

    echo "Initial iteration"
    
    # 1. target_dir  2. articles_dir  3. model_dir  4. itname
    # start at 1, 0 would be the initial model
    iterationStep $iterations_dir/it01 \
                  $articles_dir \
                  $initial_model \
                  it01
    i=1
    while [[ $i -lt $n ]]; do
        prev_itname=$(printf "it%02d" $i)
        let i=i+1
        itname=$(printf "it%02d" $i)
        echo "Starting iteration $itname"
        iterationStep $iterations_dir/$itname $articles_dir \
                      $iterations_dir/$prev_itname/model_parameters/${prev_itname}.cd_cont_4000 $itname
    done
}


# all sphinxtrain stages
# comp_feat,verify,g2p_train,lda_train,mllt_train,vector_quantize,falign_ci_hmm,force_align,vtln_align,ci_hmm,cd_hmm_untied,buildtrees,prunetree,cd_hmm_tied,lattice_generation,lattice_pruning,lattice_conversion,mmie_train,deleted_interpolation,decode


printHelp() {
    echo "Options:"
    echo "-l, --language <english|german|dutch>  which language to use. default: german"
    echo "-f, --ffmpeg <path>              path to an ffmpeg binary, if its not in PATH"
    echo "-g, --g2p <path>                 path to a g2p model required by sequitur"
    echo "-c, --caps                       flag if the model is in all caps"
    echo "-m, --model <path>               path to the initial model to use "
    echo "-s, --ser <path>                 path to the ser file required by the aligner"
    echo "-d, --dic <path>                 path to the dic file required by the aligner"
    echo ""
    echo "This script is very error prone, but might still serve as a rough guidance"
    echo "to do the whole process manually."
}

isInSet () {
    local e
    for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
    return 1
}

while [[ $# > 0 ]]; do
    key="$1"
    case $key in
        -h|--help)
            printHelp
            exit 0
            ;;
        -l|--language)
            language="$2"
            if ! isInSet "$language" "english" "german" "dutch"; then
                echo >&2 "Error: Only 'english', 'german' and 'dutch' are supported languages"
                exit 1
            fi
            shift 2
            ;;
        -f|--ffmpeg)
            ffmpeg="$2"
            shift 2
            ;;
        -g|--g2p)
            G2P_model="$2"
            shift 2
            ;;
        -c|--caps)
            G2P_model_CAPS=true
            shift
            ;;
        -m|--model)
            model="$2"
            shift 2
            ;;
        -i|--iterationsdir)
            iterationsdir="$2"
            shift 2
            ;;
        -a|--articles)
            articlesdir="$2"
            shift 2
            ;;
        *)
            ;;
    esac
done

if [ "$G2P_model" = "" ]; then
    echo "a G2P model is required"
    exit 1
fi

bootstrap $iterationsdir $articlesdir $model 3
