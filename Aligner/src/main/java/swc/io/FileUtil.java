package swc.io;
import java.io.*;

public final class FileUtil
{
	private FileUtil(){}


	public static String ReadAllText(File file) throws IOException {
        int length = 512;
        length = (int)file.length();
        try(FileInputStream inputStream = new FileInputStream(file))
        {
            return ReadAllText(inputStream, length);
        }
    }

    public static String ReadAllText(InputStream inputStream, int length)
    {
        if(length < 512)
            length = 512;
        StringBuilder stringBuilder = new StringBuilder(length);
        try {
            Reader r = new InputStreamReader(inputStream, "UTF-8");
            int c = 0;
            while ((c = r.read()) != -1) {
                stringBuilder.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return stringBuilder.toString();
    }

    public static void WriteAllText(File file, String text) throws IOException
    {
        try(FileOutputStream output = new FileOutputStream(file))
        {
        	output.write(text.getBytes("UTF-8"));
        }
    }
}
