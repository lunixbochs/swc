#! /bin/bash

# downloads and installs sphinxtrain and its dependencies in the
# directory given via $1
# temporary files may be downloaded to the current working directory

install_path="/usr/local/"


install() {
    echo "installing to $install_path"
    
    echo "downloading"
    [ ! -f sphinxbase-5prealpha.tar.gz ] && wget "https://sourceforge.net/projects/cmusphinx/files/sphinxbase/5prealpha/sphinxbase-5prealpha.tar.gz"
    [ ! -f pocketsphinx-5prealpha.tar.gz ] && wget "https://sourceforge.net/projects/cmusphinx/files/pocketsphinx/5prealpha/pocketsphinx-5prealpha.tar.gz"
    [ ! -f sphinxtrain-5prealpha.tar.gz ] && wget "https://sourceforge.net/projects/cmusphinx/files/sphinxtrain/5prealpha/sphinxtrain-5prealpha.tar.gz"
    
    echo "unpacking"
    tar -xzf sphinxbase-5prealpha.tar.gz
    tar -xzf pocketsphinx-5prealpha.tar.gz
    tar -xzf sphinxtrain-5prealpha.tar.gz
    
    echo "making sphinxbase"
    cd sphinxbase-5prealpha
    ./configure "--prefix=$install_path"
    make clean && make && make install
    cd ..
    
    echo "making pocketsphinx"
    cd pocketsphinx-5prealpha
    ./configure "--prefix=$install_path"
    make clean && make && make install
    cd ..
    
    echo "making sphinxtrain"
    cd sphinxtrain-5prealpha
    ./configure "--prefix=$install_path"
    make clean && make && make install
    cd ..
    
    cat >$install_path/SOURCEME <<EOF
export PATH=$install_path/bin:\$PATH
export LD_LIBRARY_PATH=$install_path/lib
export PKG_CONFIG_PATH=$install_path/lib/pkgconfig
EOF
}

clean() {
    echo "removing temporary install files in working directory"
    rm -r sphinxbase-5prealpha pocketsphinx-5prealpha sphinxtrain-5prealpha \
       sphinxbase-5prealpha.tar.gz pocketsphinx-5prealpha.tar.gz sphinxtrain-5prealpha.tar.gz
}

printHelp() {
    echo "Options:"
    echo "-i, --install      activate install"
    echo "-c, --clean        activate removing of temp files after install"
    echo "-p, --path <dir>   change path to install to. default is $install_path"
    echo ""
    echo "the installation will generate a SOURCEME file which needs to be sourced"
    echo "in order for the software to work properly"
}


exec_install=false
exec_clean=false


while [[ $# > 0 ]]; do
    key="$1"
    case $key in
        -h|--help)
            printHelp
            exit 0
            ;;
        -i|--install)
            exec_install=true
            shift
            ;;
        -c|--clean)
            exec_clean=true
            shift
            ;;
        -p|--path)
            install_path="$(pwd)/$2"
            shift 2
            ;;
        *)
            ;;
    esac
done

echo "install:      $exec_install"
echo "clean:        $exec_clean"
echo "install path: $install_path"

if [ $exec_install == "true" ]; then
    echo "in install"
    install
fi
if [ $exec_clean == "true" ]; then
    echo "in clean"
    clean
fi
