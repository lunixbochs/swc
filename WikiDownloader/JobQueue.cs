﻿using System;
using System.Threading;

namespace WikiDownloader
{
    class JobQueue
    {
        Thread[] _threads;
        EventWaitHandle _wh;
        LockFreeQueue<Action> _queue;
        volatile bool _isClosed = false;

        public JobQueue(int numThreads)
        {
            _queue = new LockFreeQueue<Action>();
            _wh = new EventWaitHandle(false, EventResetMode.AutoReset);
            _threads = new Thread[numThreads];
            for(int i=0;i<numThreads;i++)
            {
                _threads[i] = new Thread(Run);
                _threads[i].Start();
            }
        }

        private void Run()
        {
            bool @continue = true;
            while(@continue && !_isClosed)
            {
                try
                {
                    _wh.WaitOne();
                }
                catch { @continue = false; }

                Action work;
                while(_queue.Dequeue(out work))
                {
                    work();
                }
            }
            try
            {
                _wh.Set();//cause the next thread to exit
            }
            catch { }
        }

        public void Enqueue(Action work)
        {
            _queue.Enqueue(work);
            _wh.Set();
        }

        public void Close()
        {
            _isClosed = true;
            _wh.Set();
            foreach (var thread in _threads)
            {
                try
                {
                    thread.Join();
                }
                catch { }
            }
            try
            {
                _wh.Close();
            }
            catch { }
        }
    }

    public class LockFreeQueue<T>
    {
        public LockFreeQueue()
        {
            first = new Node();
            last = first;
        }

        Node first;
        Node last;

        public void Enqueue(T item)
        {
            Node newNode = new Node();
            newNode.item = item;
            Node old = Interlocked.Exchange(ref first, newNode);
            old.next = newNode;
        }

        public bool Dequeue(out T item)
        {
            Node current;
            do
            {
                current = last;
                if (current.next == null)
                {
                    item = default(T);
                    return false;
                }
            }
            while (current != Interlocked.CompareExchange(ref last, current.next, current));
            item = current.next.item;
            current.next.item = default(T);
            return true;
        }

        class Node
        {
            public T item;
            public Node next;
        }
    }
}
