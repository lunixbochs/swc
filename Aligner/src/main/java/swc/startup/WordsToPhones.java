package swc.startup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.Context;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.result.WordResult;

public class WordsToPhones
{
    static class WordWithTiming implements Comparable<WordWithTiming>
    {
        public String word;
        public long start;
        public long end;

        public WordWithTiming(String word, long start, long end)
        {
            this.word = word;
            this.start = start;
            this.end = end;
        }

        @Override
        public int compareTo(WordWithTiming other)
        {
            return Long.compare(this.start, other.start);
        }
    }

    public static void main(String[] args) throws Exception
    {
        URL audioUrl = new File(args[0]).toURI().toURL();
        File alignment_file = new File(args[1]);
        String acousticModelPath = args[2];
        String dictionaryPath = args[3];
        String g2pPath = args[4];

        final List<WordWithTiming> words = readWords(alignment_file);
        
        if (words.size() == 0)
        {
            System.err.println("ERROR: no timing information given");
            System.exit(-1);
        }
        Collections.sort(words);

        Configuration config = new Configuration();
        config.setAcousticModelPath(acousticModelPath);
        config.setDictionaryPath(dictionaryPath);
        Context context = new Context(config);
        int beam_width = 350;
        context.setGlobalProperty("absoluteBeamWidth", beam_width);
        context.setLocalProperty("activeList->absoluteBeamWidth", beam_width);
        context.setLocalProperty("dictionary->allowMissingWords", "true");
        context.setLocalProperty("dictionary->createMissingWords", "true");
        context.setLocalProperty("dictionary->g2pModelPath", g2pPath);
        context.setLocalProperty("dictionary->g2pMaxPron", "2");
        context.setLocalProperty("decoder->searchManager", "allphoneSearchManager");
        context.setLocalProperty("allphoneLinguist->useContextDependentPhones", true);
        context.setSpeechSource(audioUrl.openStream());
        Recognizer recognizer = context.getInstance(Recognizer.class);
        recognizer.allocate();
        Result result;
        
        
        class iterator{
            int word_index = 0;
            public WordWithTiming word = words.get(word_index);
            public List<String> phones_of_word = new ArrayList<String>();
            
            boolean goToNextWord(){
                if(word != null && phones_of_word.size() > 0)
                {
                    print(word, phones_of_word);
                }
                
                word_index++;
                phones_of_word.clear();
                if(word_index < words.size())
                {
                    word = words.get(word_index);
                    return true;
                }
                else
                {
                    word = null;
                    return false;
                }
            }

        }
        iterator iterator = new iterator();
        
        recognition:
        while ((result = recognizer.recognize()) != null)
            for (WordResult r : result.getTimedBestResult(true))
            {
                long time = (r.getTimeFrame().getStart()+r.getTimeFrame().getEnd())/2;
                while(iterator.word.end <= time)
                {
                    if(!iterator.goToNextWord())
                        break recognition;
                }
                if(iterator.word.start > time)
                    continue;
                iterator.phones_of_word.add(r.getWord().getSpelling());
            }
        iterator.goToNextWord();
    }
    
    private static void print(WordWithTiming word, List<String> phones_of_word)
    {
        System.out.print(word.word);
        System.out.print('\t');
        for(int i=0;i<phones_of_word.size();i++)
        {
            if(i > 0)
                System.out.print(' ');
            System.out.print(phones_of_word.get(i));
        }
        System.out.println();
    }

    private static List<WordWithTiming> readWords(File alignment_file) throws IOException
    {
        List<WordWithTiming> words = new ArrayList<>();

        Pattern p1 = Pattern.compile("^([^\\t]+)\\t([^\\t]+)\\t(.*)$");
        Pattern p2 = Pattern.compile("^\\+? +(.*)\\[(.*):(.*)\\]\\s*?$");
        BufferedReader reader = new BufferedReader(new FileReader(alignment_file));
        String line;
        while ((line = reader.readLine()) != null)
        {
            String word = null;
            long start = 0, end = 0;
            java.util.regex.Matcher m = p1.matcher(line);
            if (m.matches())
            {
                word = m.group(3);
                start = (long)(Double.parseDouble(m.group(1).trim())*1000);
                end = (long)(Double.parseDouble(m.group(2).trim())*1000);
            } else
            {
                m = p2.matcher(line);
                if (m.matches())
                {
                    word = m.group(1);
                    start = Long.parseLong(m.group(2).trim());
                    end = Long.parseLong(m.group(3).trim());
                }
            }
            if (word != null)
            {
                words.add(new WordWithTiming(word.trim(), start, end));
            }
        }
        reader.close();
        return words;
    }
}
