﻿using System;
using System.ComponentModel;

namespace WikiDownloader
{
    [Serializable]
    public class PageMetadata
    {
        public PageMetadata()
        {
            revision = -1;
            pageid = -1;
        }

        [DefaultValue((long)-1)]
        public long revision { get; set; }
        [DefaultValue((long)-1)]
        public long pageid { get; set; }
        [DefaultValue(null)]
        public string title { get; set; }
        [DefaultValue(null)]
        public string canonicalurl { get; set; }
        [DefaultValue(null)]
        public string identifier { get; set; }
        [DefaultValue(null)]
        public string language { get; set; }
    }

    [Serializable]
    public class AudioMetadata
    {
        public AudioMetadata()
        {
            revision = -1;
            pageid = -1;
        }

        [DefaultValue((long)-1)]
        public long revision { get; set; }
        [DefaultValue((long)-1)]
        public long pageid { get; set; }
        [DefaultValue(null)]
        public string metadataURL { get; set; }
        [DefaultValue(null)]
        public string title { get; set; }
        [DefaultValue(null)]
        public string repository { get; set; }
        [DefaultValue(null)]
        public string downloadurl { get; set; }
        [DefaultValue(null)]
        public string timestamp { get; set; }
    }

    public class ExtractedData
    {
        public ExtractedData()
        {
            article_oldid = -1;
        }

        [DefaultValue(null)]
        public string reader { get; set; }
        [DefaultValue(null)]
        public string date_read { get; set; }
		// whether we could parse the date and have done so
		[DefaultValue(false)]
        public bool is_date_normalized { get; set; }

        [DefaultValue(null)]
        public string[] audio_file_titles { get; set; }

        [DefaultValue((long)-1)]
        public long article_oldid { get; set; }
        [DefaultValue((long)-1)]
        public int oldid_quality { get; set; }
        [DefaultValue(null)]
        public string oldURL { get; set; }
    }

    [Serializable]
    public class SpokenArticleInfo
    {
        public SpokenArticleInfo()
        {
            article = new PageMetadata();
            article_parsed = new ExtractedData();
            audio_files = new AudioMetadata[0];
            audio_file_parsed = new ExtractedData();
        }

        public string error { get; set; }
        public PageMetadata article { get; set; }
        public ExtractedData article_parsed { get; set; }
        public AudioMetadata[] audio_files { get; set; }
        public ExtractedData audio_file_parsed { get; set; }

        public long GetOldId()
        {
            // give preference to article ids because sometimes the date in the audio_page is the upload date and
            // not the date it is spoken.  It is even worse if the file has been moved to commons and that
            // date is encoded.
            if (article_parsed.oldid_quality >= audio_file_parsed.oldid_quality)
                return article_parsed.article_oldid;
            return audio_file_parsed.article_oldid;
        }

		public string GetReader() {
			if (article_parsed.reader != null && !article_parsed.reader.Equals (""))
				return article_parsed.reader;
			else if (audio_file_parsed.reader != null && !audio_file_parsed.reader.Equals (""))
				return audio_file_parsed.reader;
			else
				return "__unknown_reader";
			
		}
    }
}
