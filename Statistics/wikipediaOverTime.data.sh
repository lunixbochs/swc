#!/bin/bash

if [[ ( ! $# -eq 1 )  && ( ! $# -eq 2 ) ]]; then
    echo usage: $0 "dir_containing_articles [date source]" >&2
    cat <<EOF >&2
    where [date source] is either audio or article
        audio -> date fetched from wikimedia commons is used (default)
        article -> date from the template in the article is used

    you should use "article" for the English Wikipedia as the dates in
    wikimedia commons are sometimes incorrect.  In 2007, media data was
    moved from enwp to commons and the transition date is marked instead
    of the original date.
EOF
    exit 1
fi

article_dir=$1
dsource=${2:-audio}

if [[ (  $dsource != audio )  && (  $dsource != article ) ]]; then
    echo 'date source has to be "article" or "audio", but is' $dsource >&2
    exit 1
fi

ls -1 "$article_dir" | \
    while read dir; do
        if [ \( -f "$article_dir/$dir/audio.wav" \) -a \( "$article_dir/$dir/info.json" \) ]; then
            if [[ $1 == "article" ]]; then # get timestamp from article metadata
                TIME=`python -mjson.tool "$article_dir/$dir/info.json" | grep '"date_read": "2' | sed 's/.*"date_read": "//; s/",//' | tail -1 | tr -d '\n'`
            else # get timestamp from audio file metadata
                TIME=`python -mjson.tool "$article_dir/$dir/info.json" | grep '"timestamp": "2' | sed 's/.*"timestamp": "//; s/",//' | tail -1 | tr -d '\n'`
            fi
            DUR=`soxi -D "$article_dir/$dir/audio.wav"`
            if [ \( -n "$TIME" \) -a \( -n "$DUR" \) ]; then
                echo "$TIME	$DUR"
            fi
        fi
    done

