#! /usr/bin/env python3

# Copyright (c) 2015, Arne Koehn <koehn@informatik.uni-hamburg.de>
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.


import json
import os
from argparse import ArgumentParser
from lxml import etree as ET

argp = ArgumentParser(description="Reads converts etc. aligned data")
argp.add_argument("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="print more infos")
argp.add_argument("-i", "--indirs",
                  required = True,
                  nargs = "+",
                  help="Where to read the input from (directory containing steps dir)")
argp.add_argument("-o", "--outdir",
                  required = True,
                  help="Where to store the output")
# argp.add_argument("-f", "--format", default="json",
#                   help="Which output format to use"
#                   "values: json (default)")


args = argp.parse_args()


def convert_to_xml(in_json):
    root = ET.Element("alignment")
    result = ET.ElementTree(root)
    words = in_json["words"]
    for sentence in in_json["sentences"]:
        sentence_xml = ET.SubElement(root, "sentence")
        for word in words[sentence["start_word"] : sentence["end_word"]]:
            word_xml = ET.SubElement(sentence_xml, "word")
            word_xml.text = word["original"]
            word_xml.set("normalized", word["normalized"])
            if "start" in word:
                if not "stop" in word:
                    raise Error("start but not stop in word " + str(word))
                word_xml.set("start", str(word["start"]))
                word_xml.set("stop", str(word["stop"]))
    return result
        
    
def read_from_dir(in_dir):
    in_file = in_dir + "/steps/align.data.json"
    if (not os.path.exists(in_file)):
        raise IOError("align.data.json not found in " + in_dir)
    in_json = None
    with open(in_file) as f:
        in_json = json.load(f)
    return in_json


if __name__ == "__main__":
    for in_dir in args.indirs:
        article = os.path.basename(in_dir)
        try:
            in_json = read_from_dir(in_dir)
        except IOError as e:
            print(e)
            continue
        out_data = convert_to_xml(in_json)
        out_file = args.outdir + "/" + article+".xml"
        with open(out_file, "w") as f:
            f.write(ET.tostring(out_data, encoding="unicode", pretty_print=True))
