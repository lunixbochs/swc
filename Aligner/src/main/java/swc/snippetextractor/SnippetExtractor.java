package swc.snippetextractor;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.sound.sampled.UnsupportedAudioFileException;

import edu.cmu.sphinx.util.TimeFrame;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import swc.data.SwcXmlUtil;
import swc.io.DataIO;
import swc.snippetextractor.vad.VadAnnotator;
import swc.io.FileUtil;


public class SnippetExtractor
{
    protected class Word
    {
        public Word()
        {
            start = end = Double.NaN;
        }

        public Word(Element token, int index) {
            if (sxu.getTiming(token) != null) {
                this.start = sxu.getTiming(token).getStart() / 1000.0;
                this.end = sxu.getTiming(token).getEnd() / 1000.0;
            } else {
                this.start = Double.NaN;
                this.end = Double.NaN;
            }
            this.normalized = token.getAttributeValue(SwcXmlUtil.PRONUNCIATION_ATTR);
            this.index = index;
        }
        
        public double start;  // in seconds! Not ms
        public double end;
        public String normalized;
        public int    index;
        
        public boolean hasTiming()
        {
            return !Double.isNaN(start);
        }
        
        public boolean hasContent()
        {
            return normalized != null && normalized.length() > 0;
        }
        
        @Override
        public String toString()
        {
            // TODO Auto-generated method stub
            return normalized + " [" + start + " : " + end + "]";
        }
    }
    
    protected static class Group
    {
        public Group(int start_word, int stop_word, double start_time, double end_time)
        {
            this.start_word = start_word;
            this.end_word = stop_word;
            this.start_time = start_time;
            this.end_time = end_time;
        }
        
        public int start_word;
        public int end_word;
        // times are in seconds here
        public double start_time;
        public double end_time;
        public boolean isEmpty()
        {
            return end_word <= start_word;
        }
        
        public double duration()
        {
            return end_time - start_time;
        }

        public TimeFrame getTimeFrame() {
            return new TimeFrame(Math.round(start_time * 1000), Math.round(end_time * 1000));
        }
    }


    /**
     * A sequence of words, such as a sentence.
     * start, end refer to indices in the whole sequence of words.
     * start inclusive, end exclusive.
     */
    /*
    public static class SubSequence
    {
        public SubSequence(int start, int end)
        {
            this.start = start;
            this.end = end;
        }
        
        public int start;
        public int end;
    }
    */
    
    private static File OUTPUT_DIR;
    
    private final File audio_file;
    private final Word[] words;
    private final File output_dir;
    protected final SwcXmlUtil sxu;
    /**
     * The snippetNameFormat can contain one format string for an integer
     */
    private final String snippetNameFormat = "%04d";
    
    public SnippetExtractor(File audioFile, Document document, File outputDir) throws IOException
    {
        assert audioFile.isFile();
        assert document != null;
        assert audioFile.exists();
        assert outputDir.isDirectory();
        assert outputDir.exists();

        sxu = new SwcXmlUtil(document); // TODO add xml document here

        List<Element> tokens = sxu.getNormalizedTokens();
        words = new Word[tokens.size()];
        for (int i = 0; i < tokens.size(); i++) {
            Element t = tokens.get(i);
            words[i] = new Word(t, i);
        }

        // audio file
        audio_file = audioFile;
        
        // output dir
        output_dir = outputDir;
        OUTPUT_DIR = output_dir;
        
        // TODO when to do this (probably not just here)
        try (OutputStream out = new FileOutputStream(new File(output_dir, "words.lab"))) {
            // TODO FIX timeframes are NaN NaN
            DataIO.writeLabFile(sxu.getNormalizedTokens().stream()
                    .filter(sxu::hasCompleteTiming)
                    .map(sxu::toAnnotation)
                    .collect(Collectors.toList()), out);
        }
    }
    
    public void generateTrainingSnippets() throws UnsupportedAudioFileException, IOException {

        List<TimeFrame> speechRegions = VadAnnotator.annotate(audio_file.toURI().toURL());
        try (OutputStream out = new FileOutputStream(new File(output_dir, "vad.lab"))) {
            DataIO.writeVadLabFile(speechRegions, out);
        }
        Word[] recognized_words = getRecognizedWords();
        List<Group> possible_groups = findPossibleGroupsWithVad(recognized_words, speechRegions);
        // Write annotations for the possible groups
        try (OutputStream out = new FileOutputStream(new File(output_dir, "possible.lab"))) {
            DataIO.writeEnumerationLabFile(possible_groups.stream().map(g -> g.getTimeFrame()).collect(Collectors.toList()),
                    snippetNameFormat, out);
        }

        mergeOverlappingGroups(recognized_words, possible_groups);        
        removeEmptyGroups(possible_groups);
        List<Group> groups = filterPossibleGroups(words, recognized_words, possible_groups);

        // Write annotations for the groups
        try (OutputStream out = new FileOutputStream(new File(output_dir, "snippets.lab"))) {
            DataIO.writeEnumerationLabFile(groups.stream().map(g -> g.getTimeFrame()).collect(Collectors.toList()),
                    snippetNameFormat, out);
        }

        // finally write .txt and .wav snippet files to file
        writeGroupsToFilesystem(groups);
        finishedWritingCallback();
    }

    protected void finishedWritingCallback() {}

    private String getSentenceInfoForFile(Element sentence) {
        assert SwcXmlUtil.SENTENCE_TAG.equals(sentence.getName());
        StringBuilder sb = new StringBuilder();
        for (Element t : sxu.getDescendants(sentence, Filters.element(SwcXmlUtil.TOKEN_TAG)))
            sb.append(sxu.getTokenText(t) + " ");
        sb.append("\n");
        for (Element p : sxu.getDescendants(sentence, Filters.element(SwcXmlUtil.PRONUNCIATION_TAG)))
            sb.append(p.getAttributeValue(SwcXmlUtil.PRONUNCIATION_ATTR) + " ");
        sb.append("\n");
        // I dropped the annotated part.
        // Proper output needs to be decided if someone actually wants to use this.
        // This is just a placeholder mainly
        return sb.toString();
    }

    // TODO integrate logic from findGroupsFromSentences into this method
    public void generateSentenceSnippets() throws IOException {

        List<Element> alignedSentences = sxu.getSentenceLikeElements().stream()
                .filter(sxu::hasSpokenParts)
                .filter(sxu::hasCompleteTiming)
                .collect(Collectors.toList());
        // each sentence contains at least one pronounciation token and all pronounciation tokens in the sentence have timings.

        // Write snippets lab file
        try (OutputStream out = new FileOutputStream(new File(output_dir, "snippets.lab"))) {
            DataIO.writeEnumerationLabFile(alignedSentences.stream().map(sxu::getAggregateTiming).collect(Collectors.toList()),
                    snippetNameFormat, out);
        }

        int counter = 0;
        for (Element s : alignedSentences) {
            String snippetName = String.format(snippetNameFormat, counter);
            counter += 1;

            // .txt file
            String text = getSentenceInfoForFile(s);
            FileUtil.WriteAllText(new File(output_dir, snippetName + ".txt"), text);

            // .wav file
            TimeFrame tf = sxu.getAggregateTiming(s);
            try {
                createAudioSnippet(tf, new File(output_dir, snippetName + ".wav"));
            } catch (Exception e) {
                // TODO poor exception handling
                System.err.println("Error while executing ffmpeg:");
                System.err.println(e.getMessage());
            }
        }
    }

    public String normalizedString(Word[] group_words) {
        String result = "";
        for(int j = 0; j < group_words.length;j++)
        {
            Word word = group_words[j];
            result += word.normalized + " ";
        }
        return result.replaceAll("\\s+", " ").trim();
    }

    protected void writeGroupsToFilesystem(List<Group> groups) throws IOException {
    	int i=0;
	    for(Group group: groups)
	    {
	        Word[] group_words = Arrays.copyOfRange(words, group.start_word, group.end_word);

	        String normalized = normalizedString(group_words);


	        double start = group.start_time;
	        double stop = group.end_time;

            doWriteGroup(i, group_words, normalized, start, stop);
            i++;
	    }
    }

    protected void doWriteGroup(int i, Word[] group_words, String normalized, double start, double stop) throws IOException {
        String name = String.format(snippetNameFormat, i);

        System.err.print(name);
        String annotated = String.format("[%.3f to %.3f] ", start,stop);
        for(Word w:group_words)
            annotated += String.format("[%.3f] %s [%.3f] ", w.start,w.normalized,w.end);
        System.err.println(annotated);

        File output_file = new File(output_dir, name+".wav");
        FileUtil.WriteAllText(new File(output_dir,name+".txt"), "<s> " + normalized+ " </s>"); // TODO this is bullshit
        try
        {
            createAudioSnippet(start, stop, output_file);
        } catch (Exception e)
        {
            System.err.println("Error while executing ffmpeg:");
            System.err.println(e.getMessage());
        }
    }
    /**
    private static void writeWordsLabelFile(Word[] words, File file) throws IOException
    {
    	StringBuilder sb = new StringBuilder();
    	for (int i = 0; i < words.length; i++)
    	{
    		Word word = words[i];
    		String label = String.format("%.3f %.3f %s\n", word.start, word.end, word.normalized);
    		sb.append(label);
    	}
    	FileUtil.WriteAllText(file, sb.toString());
    }
    
    private static void writeSnippetsLabelFile(List<Group> snippets, File file) throws IOException
    {
    	StringBuilder sb = new StringBuilder();
    	int i = 0;
    	for (Group g : snippets)
    	{
    		sb.append(String.format("%.3f %.3f %04d\n", g.start_time, g.end_time, i));
    		i += 1;
    	}
    	FileUtil.WriteAllText(file, sb.toString());
    }

    private static List<Group> findGroupsFromSentences(Word[] words, SubSequence[] sentences)
    {
        List<Group> groups = new ArrayList<SnippetExtractor.Group>();
        for(SubSequence sentence:sentences)
        {
            if(allWordsHaveTimings(words, sentence))
            {
                double start_time = Double.POSITIVE_INFINITY, end_time = Double.NEGATIVE_INFINITY;
                for(int i=sentence.start; i<sentence.end;i++)
                {
                    Word w = words[i];
                    if(start_time > w.start)
                        start_time = w.start;
                    if(end_time < w.end)
                        end_time = w.end;
                }
                
                double group_start_time;                
                int previous = sentence.start-1;
                while(previous >= 0 && !words[previous].hasContent())
                    previous--;
                if(previous >= 0)
                    group_start_time = (words[previous].end + start_time)*0.5;
                else
                    group_start_time = Math.max(0, start_time-0.1);
                group_start_time = Math.max(start_time - 0.5, group_start_time);
                
                double group_end_time;
                int next = sentence.end;
                while(next < words.length && !words[next].hasContent())
                    next++;
                if(next < words.length)
                    group_end_time = (words[next].start + end_time)*0.5;
                else
                    group_end_time = end_time + 0.1;
                group_end_time = Math.min(end_time + 0.5, group_end_time);
                
                groups.add(new Group(sentence.start, sentence.end, group_start_time, group_end_time));
            }
        }
        return groups;
    }

    private static boolean allWordsHaveTimings(Word[] words, SubSequence sentence)
    {
        for(int i = sentence.start; i < sentence.end; i++)
        {
            if(words[i].hasContent()&& !words[i].hasTiming())
                return false;
        }
        int previous = sentence.start-1;
        while(previous >= 0 && !words[previous].hasContent())
            previous--;
        if(previous >= 0 && words[previous].hasContent() && !words[previous].hasTiming())
            return false;
        
        int next = sentence.end;
        while(next < words.length && !words[next].hasContent())
            next++;
        if(next < words.length && words[next].hasContent() && !words[next].hasTiming())
            return false;
        
        return true;
    }
    **/
    
    /**
     * The time frames from VAD are used to find longer periods of speech.
     * They are then combined with the recognized words, to find speech periods
     * where the content is recognized. This also eliminates false positives by the VAD.
     */
    private static List<Group> findPossibleGroupsWithVad(Word[] recognized_words, List<TimeFrame> speechRegions)
	{
	    List<Group> possible_groups = new ArrayList<SnippetExtractor.Group>();
	    
	    //Find words within the time frames
	    for(TimeFrame region : speechRegions)
	    {
	        double regionStart = region.getStart()/1000.0;
	        double regionEnd = region.getEnd() /1000.0;
	        
	        int l,r;
	        for(l = 0; l< recognized_words.length;l++)
	        {
	            Word w = recognized_words[l];
	            if(w.hasTiming() && w.end > regionStart)
	            	// only the end is looked for, because sometimes the leading words' timings contain a lot of leading silence
	                break;
	        }//l is now recognized_words the first word overlapping with the region
	        
	        for(r = recognized_words.length-1; r >= 0; r--)
	        {
	            Word w = recognized_words[r];
	            if(w.hasTiming() && (w.end+w.start)/2 < regionEnd)
	            	// middle of the word should be in, end not necessary as timings might be off a little
	                break;
	        }
	        
	        if(l<=r)
	            possible_groups.add(new Group(l, r+1, regionStart, regionEnd));
	    }
	    return possible_groups;
	    
	}

	private static void mergeOverlappingGroups(Word[] recognized_words, List<Group> possible_groups) throws IOException
	{
	    int mergeCounter = 0;
	    //Take care of overlaps:
	    for(int i = 1; i<possible_groups.size();i++)
	    {
	        find_overlaps:while(i < possible_groups.size())
	        {
	            Group group1 = possible_groups.get(i-1);
	            Group group2 = possible_groups.get(i);
	            
	            if(group1.end_word <= group2.start_word)
	                break; //nothing to do here => continue with next
	            
	            int group1_end = group2.start_word;
	            for(int j = group2.start_word; j < group1.end_word; j++)
	            {
	                Word w = recognized_words[j];
	                double overlap1 = group1.end_time - w.start;
	                double overlap2 = w.end - group2.start_time;
	                if(overlap1 > 0.3 && overlap2 > 0.3)
	                {
	                    //merge regions:
	                    possible_groups.set(i-1, new Group(
	                            group1.start_word, group2.end_word,
	                            group1.start_time, group2.end_time));
	                    possible_groups.remove(i);
	                    mergeCounter += 1;
	                    continue find_overlaps;
	                }
	                
	                if(overlap1 > overlap2)
	                    group1_end = j+1;
	            }
	            
	            group1.end_word = group1_end;
	            group2.start_word = group1_end;
	            
	            break;//continue with next
	        }
	    }
	
	    FileUtil.WriteAllText(new File(OUTPUT_DIR, "overlap"), new Integer(mergeCounter).toString());
	}

	private static void removeEmptyGroups(List<Group> possible_groups)
	{
	    //remove empty groups:
	    for(int i=possible_groups.size(); i --> 0;)
	    {
	        if(possible_groups.get(i).isEmpty())
	            possible_groups.remove(i);
	    }
	}

	private static List<Group> filterPossibleGroups(Word[] words, Word[] recognized_words, List<Group> possible_groups)
    {
        List<Group> result = new ArrayList<SnippetExtractor.Group>();
        for(int i=0;i<possible_groups.size();i++)
        {
            Group g = possible_groups.get(i);
            // groups shorter than 0.6 seconds are discarded
            if(g.isEmpty() || !(g.duration() > 0.6)) // TODO parameter should not be hardcoded
                continue;
            
            //map indices back: recognized_words --> words
            int start_word = recognized_words[g.start_word].index;
            int end_word = recognized_words[g.end_word-1].index+1;

            // We check for the boundaries:  If there are words before or after the current group that
            // are not successfully aligned, skip the group (because maybe the alignment is too noisy)

            //check words before:
            if(i-1 >= 0)
            {
            	// previous group, last word
                Word previous_last_word = recognized_words[possible_groups.get(i-1).end_word - 1];
                if(!allWordsHaveTimings(words, previous_last_word.index+1, start_word))
                    continue;
            }
            else
            {
                if(!allWordsHaveTimings(words, 0, start_word))
                    continue;
            }
            //check words afterwards
            if(i+1 < possible_groups.size())
            {
                Word next_first_word = recognized_words[possible_groups.get(i+1).start_word];
                if(!allWordsHaveTimings(words, end_word, next_first_word.index))
                    continue;
            }
            else
            {
                if(!allWordsHaveTimings(words, end_word, words.length))
                    continue;
            }

            //if(!allWordsHaveTimings(words, start_word, end_word))
            //    continue;

            double longestGapLength = 0;
            double missingWordLength = 0;
            boolean wasMissingInBetween = false;
            double lastEnd = g.start_time;
            for(int j=start_word;j<end_word;j++)
            {
                Word w = words[j];
                if(w.hasTiming())
                {
                    double gapLength = w.start - lastEnd;
                    if(gapLength > longestGapLength)
                        longestGapLength = gapLength;
                }
                if(wasMissingInBetween && w.hasTiming())
                {
                    missingWordLength += w.start - lastEnd;
                    wasMissingInBetween = false;
                }
                if(!w.hasTiming())
                {
                    if(w.hasContent())
                        wasMissingInBetween = true;
                }
                else
                {
                    lastEnd = w.end;
                }
            }
            if(g.end_time - lastEnd > longestGapLength)
                longestGapLength = g.end_time - lastEnd;
            if(longestGapLength > 2)
                continue; //Too big gaps;
            
            if(missingWordLength > g.duration() * 0.20)
                continue; //Too much missing;

            if(g.end_time - words[end_word-1].end > 1.5)
                continue;//Too much space at the end => that might be words!
            
            result.add(new Group(start_word, end_word, g.start_time, g.end_time));
        }
        
        return result;
    }
    
    private Word[] getRecognizedWords()
    {
        //make a list containing only words with timing information
        List<Word> recognized_word_list = new ArrayList<SnippetExtractor.Word>();
        for(Word w: words)
            if(w.hasTiming())
                recognized_word_list.add(w);
        Word[] recognized_words = recognized_word_list.toArray(new Word[recognized_word_list.size()]);
        return recognized_words;
    }

    private static boolean allWordsHaveTimings(Word[] words, int start, int end)
    {
        for(int i=start;i<end;i++)
        {
            if(words[i].hasContent() && !words[i].hasTiming())
                return false;
        }
        return true;
    }

    @SuppressWarnings("unused")
    private static List<Group> findGroups(Word[] words, java.net.URL audioUrl)
    {
        //Detect pauses
        final double min_pause_length = 1.0;

        boolean[] is_pause_before = new boolean[words.length + 1];

        for (int i = 1; i < words.length; i++)
        {
            Word previous_word = words[i - 1];
            Word current_word = words[i];
            double pause_length = current_word.start - previous_word.end;
            if (pause_length > min_pause_length)
            {
                is_pause_before[i] = true;
            }
        }
        is_pause_before[0] = words[0].hasTiming();// first word
        is_pause_before[words.length] = words[words.length - 1].hasTiming();// last word

        // find groups of words surrounded by pauses
        int group_start = -1;

        List<Group> groups = new ArrayList<>();
        for (int i = 0; i < is_pause_before.length; i++)
        {
            if (is_pause_before[i])
            {
                if (group_start >= 0)
                {
                    groups.add(new Group(group_start, i, words[group_start].start - min_pause_length / 2,
                            words[i - 1].end + min_pause_length / 2));
                }
                group_start = i;
            }
            if (i < words.length && !words[i].hasTiming())
            {
                group_start = -1;
            }
        }
        return groups;
    }

    protected void createAudioSnippet(TimeFrame tf, File outputFile) throws InterruptedException, InvalidInputException, IOException {
        createAudioSnippet(tf.getStart() / 1000.0, tf.getEnd() / 1000.0, outputFile);
    }

    /**
     * Uses a system call to call 'sox', gives the input file, output file and start/stop seconds
     * directly to sox and lets sox handle it all.
     */
    protected void createAudioSnippet(double start_seconds, double stop_seconds, File output_file)
            throws InvalidInputException, IOException, InterruptedException
    {
        start_seconds = Math.max(0, start_seconds);
        // TODO cap stop_seconds at max length ... max length is not known currently.
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(3);
        nf.setGroupingUsed(false);
        String[] cmd = {
                "sox",
                audio_file.getAbsolutePath(),
                output_file.getAbsolutePath(),
                "trim",
                nf.format(start_seconds),
                "=" + nf.format(stop_seconds),
        };
        Process p = Runtime.getRuntime().exec(cmd);
        int error = p.waitFor();
        if(error != 0)
        {
            String message = FileUtil.ReadAllText(p.getErrorStream(), -1);
            throw new InvalidInputException("sox exited with code: "+error+"\nmessage:"+message);
        }
    }
}
