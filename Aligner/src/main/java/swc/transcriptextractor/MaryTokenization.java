package swc.transcriptextractor;

import com.google.common.collect.Lists;
import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import org.jdom2.*;
import org.jdom2.filter.Filters;
import org.jdom2.input.DOMBuilder;
import org.jdom2.output.DOMOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import swc.data.SwcXmlUtil;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MaryTokenization {

	private static final Logger logger = LoggerFactory.getLogger(MaryTokenization.class);

	private static final Namespace MARY_NS = Namespace.getNamespace("http://mary.dfki.de/2002/MaryXML");
	private static final String MARY_PARAGRAPH = "p";
	private static final String MARY_SENTENCE = "s";
	private static final String MARY_TOKEN = "t";
	private static final String MARY_MULTI_TOKEN_UNIT = "mtu";

	private MaryInterface marytts;
	private boolean normalize;


	/**
	 * Creates a MaryTokenitation Instance.
	 * @param lang The language used for normalization. Needs to be given even if normalize = false,
	 *             because it still affects the handling of "umlaute" (they are removed for lang = en)
	 * @param normalize If numbers and abbrevs etc. should be normalized / expanded
	 * @throws MaryConfigurationException
	 */
	public MaryTokenization(Locale lang, boolean normalize) throws MaryConfigurationException {
		this.normalize = normalize;
		marytts = new LocalMaryInterface();
		marytts.setLocale(lang);
		marytts.setInputType("RAWMARYXML");
		if (normalize)
			marytts.setOutputType("WORDS");
		else
			marytts.setOutputType("TOKENS");
		logger.info(String.format("MaryTTS Output Format Params:\n%s", marytts.getOutputTypeParams()));
	}


	// this table has been copied from toGermanLetterTable in MaryUtils.java in marytts.
	// in addition, all umlauts are mapped to the non-umlaut.
	protected static final char[] normalizationTable =
			new char[] {
					'ä', 'a',
					'ö', 'o',
					'ü', 'u',
					'Ä', 'A',
					'Ö', 'O',
					'Ü', 'U',
					'ß', 's',
					'\u00c0', 'A', // À
					'\u00c1', 'A', // Á
					'\u00c2', 'A', // Â
					'\u00c3', 'A', // Ã
					'\u00c5', 'A', // Å
					'\u00c6', 'A', // Æ
					'\u00c7', 'C', // Ç
					'\u00c8', 'E', // È
					'\u00c9', 'E', // É
					'\u00ca', 'E', // Ê
					'\u00cb', 'E', // Ë
					'\u00cc', 'I', // Ì
					'\u00cd', 'I', // Í
					'\u00ce', 'I', // Î
					'\u00cf', 'I', // Ï
					'\u00d1', 'N', // Ñ
					'\u00d2', 'O', // Ò
					'\u00d3', 'O', // Ó
					'\u00d4', 'O', // Ô
					'\u00d5', 'O', // Õ
					'\u00d8', 'O', // Ø
					'\u00d9', 'U', // Ù
					'\u00da', 'U', // Ú
					'\u00db', 'U', // Û
					'\u00dd', 'Y', // Ý
					'\u00e0', 'a', // à
					'\u00e1', 'a', // á
					'\u00e2', 'a', // â
					'\u00e3', 'a', // ã
					'\u00e5', 'a', // å
					'\u00e6', 'a', // æ
					'\u00e7', 'c', // ç
					'\u00e8', 'e', // è
					'\u00e9', 'e', // é
					'\u00ea', 'e', // ê
					'\u00eb', 'e', // ë
					'\u00ec', 'i', // ì
					'\u00ed', 'i', // í
					'\u00ee', 'i', // î
					'\u00ef', 'i', // ï
					'\u00f1', 'n', // ñ
					'\u00f2', 'o', // ò
					'\u00f3', 'o', // ó
					'\u00f4', 'o', // ô
					'\u00f5', 'o', // õ
					'\u00f8', 'o', // ø
					'\u00f9', 'u', // ù
					'\u00fa', 'u', // ú
					'\u00fb', 'u', // û
					'\u00fd', 'y', // ý
					'\u00ff', 'y', // ÿ
					8220, '"', // „
					8222, '"', // “
					8211, '-', // -
					'\u2033', '"', // ″
					'\u2036', '"', // ‶
					'\u201c', '"', // “
					'\u201d', '"', // ”
					'\u201e', '"', // „
					'\u201f', '"', // ‟
					'\u00ab', '"', // «
					'\u00bb', '"', // »
					'\u2018', '\'', // ‘
					'\u2019', '\'', // ’
					'\u201a', '\'', // ‚
					'\u201b', '\'', // ‛
					'\u2032', '\'', // ′
					'\u2035', '\'', // ‵
					'\u2039', '\'', // ‹
					'\u203a', '\'', // ›
					'\u2010', '-', // ‐
					'\u2011', '-', // ‑
					'\u2012', '-', // ‒
					'\u2013', '-', // –
					'\u2014', '-', // —
					'\u2015', '-', // ―
			};

	/**
	 * checks whether a is a result of normalizing b by marytts (or already equal)
	 */
	private boolean wasNormalizedFrom(Character a, Character b) {
		if (a.equals(b))
			return true;
		int b_index = -1;
		for (int i = 0; i < normalizationTable.length; i += 2) {
			if (normalizationTable[i] == b) {
				b_index = i;
				break;
			}
		}
		return normalizationTable[b_index+1] == a;
	}

	/**
	 * Modifies the given document, replaces all text nodes with sequences of sentences or tokens.
	 * @param structuredDoc  The document to modify.
	 */
	public void tokenize(org.jdom2.Document structuredDoc) throws SynthesisException, ParserConfigurationException, SAXException, IOException, JDOMException {
		SwcXmlUtil sxu = new SwcXmlUtil(structuredDoc, true); // override ignore; everything should be tokenized.
		// Replace all text nodes with their tokenization outputs
		List<Text> texts = sxu.getCDATA();
		String originalCDATA = texts.stream().map(Text::getText).collect(Collectors.joining());
		logger.info(String.format("Tokenizing: Replacing %d text nodes", texts.size()));
		for (Text t : texts) {
			if (t.getText().trim().isEmpty())
				continue; // Mary can't deal with whitespace only -> NPE
			Element p = t.getParentElement();
			p.setContent(p.indexOf(t), getTokenizedSwcXml(t.getText()));
		}
		// Remove sentence tags in section titles.
		List<Element> sectiontitles = sxu.getSectionTitles();
		logger.info(String.format("Tokenizing: Cleaning up %d section titles", sectiontitles.size()));
		for (Element sectiontitle : sectiontitles) {
			for (Element sentence : sxu.getDescendants(sectiontitle, Filters.element(SwcXmlUtil.SENTENCE_TAG))) {
				Element p = sentence.getParentElement();
				p.setContent(p.indexOf(sentence), sentence.removeContent());
			}
		}
		// Remove normalization from punctuation tokens.
		List<Element> tokens = sxu.getTokens();
		logger.info(String.format("Tokenizing: Found %d tokens", tokens.size()));
		Pattern p = Pattern.compile("[.,:;?!()„“\\[\\]\\-\"'_\\s↑]");
		for (Element token : tokens) {
			boolean isPunctuation = p.matcher(sxu.getTokenText(token)).matches();
			if (isPunctuation)
				token.removeContent(Filters.element(SwcXmlUtil.PRONUNCIATION_TAG));
		}
		// Add back whitespace into the document

		// There is old CDATA and new CDATA, the new CDATA is missing the whitespace.
		// It is assumed that the new CDATA is only omitting characters from the old one,
		// no characters are added.
		// Both CDATA variants are iterated and the whitespace is found and then added to
		// the new document.  There are two cases:
		// 1.  Whitespace between tokens.
		//     If whitespace between tokens is found, the whitespace is attached to the lowest
		//     common ancestor of its two neighboring tokens.
		// 2.  Whitespace inside a token.
		//     If whitespace is found inside a token, the whitespace and the character offset
		//     from the token start where it is to be inserted is saved, and later on the
		//     whitespace is inserted in each token, in reverse order to not mess with the offset
		//     in case multiple whitespaces need to be inserted in the same token.
		CDATAMapping cm = getCDATAMapping(structuredDoc.getRootElement());
		// originalCDATA saved above
		String newCDATA = cm.cdata;
		List<Text> tokenMap = cm.charTokenMapping;
		logger.debug("Old CDATA: " + originalCDATA);
		logger.debug("New CDATA: " + newCDATA);
		int oi = 0, ni = 0; // indices for cdatas
		String whitespace = "";
		List<IntraTokenWhiteSpaceInsert> inserts = new ArrayList<>();
		while (oi < originalCDATA.length()) {
			logger.debug("oi: " + oi + " ni: " + ni);

			if (ni < newCDATA.length() && wasNormalizedFrom(newCDATA.charAt(ni), originalCDATA.charAt(oi))) {
				logger.debug("identical");
				if (!whitespace.isEmpty()) {
					// CDATA is equivalent again, and some whitespace was collected to be inserted
					Text wsNode = new Text(whitespace);
					if (ni == 0) {
						// The whitespace is at the very beginning, special case
						sxu.getBodyRoot().addContent(0, wsNode);
					} else {
						Text previousText = tokenMap.get(ni - 1);
						Text nextText = tokenMap.get(ni);
						if (previousText != nextText) {  // whitespace between tokens
							logger.debug("Adding whitespace between'" + previousText.getText() + "' and '" + nextText.getText() + "'");
							insertWhitespace(previousText, nextText, wsNode);
						} else {  // whitespace inside a token, save in a list for later
							int tokenStart = tokenMap.indexOf(previousText);
							int offset = ni - tokenStart;
							inserts.add(new IntraTokenWhiteSpaceInsert(previousText, whitespace, offset));
						}
					}
					whitespace = "";
				}
				if (newCDATA.charAt(ni) != originalCDATA.charAt(oi)) {
					// some normalization by maryTTS happened, fix it
					Text currText = tokenMap.get(ni);
					int offset = ni - tokenMap.indexOf(currText);
					char[] chars = currText.getText().toCharArray();
					chars[offset] = originalCDATA.charAt(oi);
					currText.setText(new String(chars));
				}
				oi++;
				ni++;
			} else {
				logger.debug("not identical");
				whitespace += originalCDATA.charAt(oi);
				oi++;
			}
			logger.debug("whitespace: '" + whitespace + "'");
		}
		if (!whitespace.equals("")) { // Trailing whitespace was collected
			sxu.getBodyRoot().addContent(new Text(whitespace));
		}
		// 2. case, insert whitespace inside of tokens in reverse order.
		if (!inserts.isEmpty()) {
			inserts = Lists.reverse(inserts);
			Text currentToken = inserts.get(0).text;
			List<IntraTokenWhiteSpaceInsert> currentInserts = new ArrayList<>();
			for (IntraTokenWhiteSpaceInsert i : inserts) {
				if (i.text != currentToken) {
					performIntraTokenInsert(currentInserts, currentToken);
					currentToken = i.text;
					currentInserts = new ArrayList<>();
					currentInserts.add(i);
				} else {
					currentInserts.add(i);
				}
			}
			// perform insertion for last token (i.e. the first in the text)
			performIntraTokenInsert(currentInserts, currentToken);
		}
		String resultingCDATA = sxu.getCDATA().stream().map(t->t.getText()).collect(Collectors.joining());
		assert originalCDATA.equals(resultingCDATA): "generated CDATA corrupt!\n\n expected:\n"+originalCDATA+"\n\ngot:\n"+resultingCDATA;
	}

	private void performIntraTokenInsert(List<IntraTokenWhiteSpaceInsert> currentInserts, Text currentToken) {
		String newText = currentToken.getText();
		for (IntraTokenWhiteSpaceInsert j : currentInserts) {
			newText = newText.substring(0, j.offset) + j.whitespace + newText.substring(j.offset, newText.length());
		}
		logger.info("Setting new Text. Old: '" + currentToken.getText() + "' New: '" + newText + "'");
		currentToken.setText(newText);
	}
	/**
	 * Simple 'named tuple' representing whitespace which needs to be inserted into a token
	 * at a specific position.
	 */
	private static class IntraTokenWhiteSpaceInsert {
		final Text text;
		final String whitespace;
		final int offset;

		IntraTokenWhiteSpaceInsert(Text text, String whitespace, int offset) {
			this.text = text;
			this.whitespace = whitespace;
			this.offset = offset;
		}
	}

	/**
	 * Returns the path of elements from the root to the given Content element.
	 * Root and the element itself are contained in the path.  Path starts
	 * from the root.
	 */
	private static List<Content> getPathToElement(Content e) {
		List<Content> path = new ArrayList<>();
		path.add(e);
		while (e != null) {
			e = e.getParentElement();
			if (e != null)
				path.add(e);
		}
		return Lists.reverse(path); // starts with root and goes down the tree
	}

	/**
	 * Attaches the given whitespace to the lowest common ancestor of the two given Elements.
	 * The lowest common ancestor should not be one of the elements themselves.  If both
	 * Elements are 'Text', this cannot occur.  This is the usual use case.
	 * @param pElem the previous (left) neighbor of the whitespace.
	 * @param nElem the next (right) neighbor of the whitespace.
	 * @param ws the whitespace to be inserted.
	 */
	private static void insertWhitespace(Content pElem, Content nElem, Content ws) {
		// note: algorithm cannot handle pElem being the parent of nElem or the other way round
		List<Content> pPath = getPathToElement(pElem);
		List<Content> nPath = getPathToElement(nElem);
		int lowestCommonAncestorIndex = 0;
		for (int i = 0; i < nPath.size() && i < pPath.size(); i++) {
			if (pPath.get(i).equals(nPath.get(i))) {
				lowestCommonAncestorIndex = i;
			} else {
				break;
			}
		}
		Parent lowestCommonAncestor = (Parent) pPath.get(lowestCommonAncestorIndex);
		int insertIndex = lowestCommonAncestor.indexOf(pPath.get(lowestCommonAncestorIndex + 1)) + 1;
		lowestCommonAncestor.addContent(insertIndex, ws);
	}


	/**
	 * A 'named tuple' class, holding CDATA as a String, and a corresponding list
	 * of the same length, mapping each character in the CDATA to a 'Text' node,
	 * which is the Token the character belongs to.
	 */
	private static class CDATAMapping {
		final String cdata;
		final List<Text> charTokenMapping;

		CDATAMapping(String cdata, List<Text> charTokenMapping) {
			this.cdata = cdata;
			this.charTokenMapping = charTokenMapping;
		}
	}


	/**
	 * @param root The element for which the CDATA mapping should be created.
	 */
	private static CDATAMapping getCDATAMapping(Element root) {
		StringBuilder cdata = new StringBuilder();
		List<Text> charTokenMapping = new ArrayList<>();
		for (Text text : root.getDescendants(Filters.text())) {
			String s = text.getText();
			cdata.append(s);
			for (int i = 0; i < s.length(); i++) {
				charTokenMapping.add(text);
			}
		}
		return new CDATAMapping(cdata.toString(), charTokenMapping);
	}


	/**
	 * Takes a text as a single string and returns a list of sentences, as SWC nodes.
	 * @param text The complete text
	 * @return The internal data structure to represent the tokenized text; a List of Sentences
	 * @throws SynthesisException if an error with MaryTTS occurs
	 */
	private List<Element> getTokenizedSwcXml(String text) throws SynthesisException, JDOMException {
		// Prepare Mary raw XML.
		logger.debug("Tokenizing text.");
		logger.debug(String.format("Tokenizing. Original Text: %s", text));
		// Adds tags around numbers so they will be normalized as years.
		org.w3c.dom.Document rawdoc = getRawMaryXML(text);
		// Does the tokenization and normalization.
		org.w3c.dom.Document w3cDoc = marytts.generateXML(rawdoc);
		// Convert to JDOM2 object.
		Element maryRootNode = new DOMBuilder().build(w3cDoc).getRootElement();
		// Transform into SWC datastructure.
		return tranformMaryTokensToSwcTokens(maryRootNode);
	}


	/**
	 * Marks numbers which should be pronounced as years.
	 * Creates XML, but does not properly escape things. possible source of bugs.
	 */
	private org.w3c.dom.Document getRawMaryXML(String text) throws JDOMException {
		// mark year numbers
		Element maryRootNode = new Element("maryxml", MARY_NS);
		maryRootNode.setAttribute("lang", marytts.getLocale().toString(), Namespace.XML_NAMESPACE);
		maryRootNode.setAttribute("version", "0.4");
		if (normalize && marytts.getLocale().getLanguage().startsWith("de")) {
			String yearRegex = "\\W1[6789]\\d\\d[^\\w,]";
			String[] pieces = text.split(String.format("((?<=%1$s)|(?=%1$s))", yearRegex));
			for (int i = 0; i < pieces.length; i++) {
				if (pieces[i].matches(yearRegex)) {
					Element sayAs = new Element("say-as", MARY_NS);
					sayAs.setAttribute("type", "date:y");
					sayAs.setContent(new Text(pieces[i]));
					maryRootNode.addContent(sayAs);
				} else {
					maryRootNode.addContent(new Text(pieces[i]));
				}
			}
		} else {
			maryRootNode.addContent(new Text(text));
		}
		return new DOMOutputter().output(new Document(maryRootNode));
	}


	/**
	 * Just dump the whole maryxml node in here and receive SWC XML.
	 * @param maryNode A MaryXML node: t, mtu, s or anything else
	 * @return a List of SWC sentences or tokens.
	 */
	public static List<Element> tranformMaryTokensToSwcTokens(Element maryNode) {
		Map<String, String> directMapping = new HashMap<>();
		//directMapping.put(MARY_PARAGRAPH, SwcXmlUtil.PARAGRAPH_TAG);
		directMapping.put(MARY_SENTENCE, SwcXmlUtil.SENTENCE_TAG);
		List<Element> results = new ArrayList<>();
		final String name = maryNode.getName();
		if (name.equals(MARY_TOKEN)) {
			Element result = new Element("t");
			result.addContent(new Text(maryNode.getText()));
			result.addContent(pronunciationFromMaryToken(maryNode));
			results.add(result);
		} else if (name.equals(MARY_MULTI_TOKEN_UNIT)) {
			Element result = new Element("t");
			result.addContent(new Text(maryNode.getAttributeValue("orig")));
			for (Element subt : maryNode.getDescendants(Filters.element("t", MARY_NS)))
				result.addContent(pronunciationFromMaryToken(subt));
			results.add(result);
		} else if (directMapping.keySet().contains(name)) {
			Element result = new Element(directMapping.get(name));
			for (Element elem : maryNode.getChildren())
				result.addContent(tranformMaryTokensToSwcTokens(elem));
			results.add(result);
		} else {
			for (Element elem : maryNode.getChildren())
				results.addAll(tranformMaryTokensToSwcTokens(elem));
		}
		return results;
	}


	/**
	 * Just for handling something weird mary does with normalized numbers.
	 * Creates an SWC:n tag for a MARY:t tag.
	 */
	public static Element pronunciationFromMaryToken(Element t) {
		String soundsLike = t.getAttributeValue("sounds_like");
		Element result = new Element("n");
		if (soundsLike != null)
			result.setAttribute(SwcXmlUtil.PRONUNCIATION_ATTR, soundsLike);
		else
			result.setAttribute(SwcXmlUtil.PRONUNCIATION_ATTR, t.getText());
		return result;
	}


	// for debugging; writing the original MaryXML to file
	private static void writeXmlOutput(org.w3c.dom.Document doc, String filename) throws IOException, TransformerException {
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

		new File(filename).getParentFile().mkdirs();
	    try (FileWriter file = new FileWriter(filename)) {
		    transformer.transform(new DOMSource(doc), 
		         new StreamResult(file));
	    }
	}
}
