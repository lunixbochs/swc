﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Util;

namespace WikiDownloader
{
    //Don't forget to register your language in Downloader.Main in Downloader.cs
    class AudioDownloader_en : AudioDownloader
    {

		protected override string getLanguage () { return "en"; }

	    protected override ExtractedData ParseArticleXML(long pageid, XmlDocument xmlDoc)
		{
			var result = new ExtractedData { article_oldid = -1 };

			XmlElement template = GetTemplatesWithTitleContaining(xmlDoc, "Spoken Wikipedia").FirstOrDefault();
			if (template != null) {
				Log ("Debug", template.OuterXml);
				var parts = template.GetChildElements ("part").ToList ();
				if (template.GetChildElement ("title").InnerText.Trim().ToLowerInvariant ().Equals ("spoken wikipedia")) {
					result.audio_file_titles = new string[] { "File:" + parts [0].GetChildElement ("value").InnerText.TrimOrNull () };//part 0 is file name
					if (parts.Count > 1)
						result.date_read = parts [1].GetChildElement ("value").InnerText.TrimOrNull ();//part 1 is date
				} else if (parts.Count () > 1) { // most likely these are "Spoken Wikipedia-N" with N \in 1..9
					var templateName = template.GetChildElement("title").InnerText; 
					var numPartsStr = templateName.Substring(templateName.IndexOf("-")+1);
					int numParts;
					if (!int.TryParse(numPartsStr, out numParts) || numParts < 1 || numParts > parts.Count() -1)
						numParts = parts.Count() - 1;
					result.date_read = parts [0].GetChildElement ("value").InnerText.TrimOrNull ();//part 0 is date
					var audioFileTitles = new List<string>();
					for (int i = 1; i < numParts + 1; i++)
					{
						var audioFileTitle = parts[i].GetChildElement("value").InnerText.TrimOrNull();
						// there might be empty parts due to misformed template input, e.g. a | at the end
						// ignore those parts.
						if (audioFileTitle.Length > 2)
							audioFileTitles.Add("File:" + audioFileTitle);
					}
					result.audio_file_titles = audioFileTitles.ToArray();
				} else {
					Log ("ERROR", "Can't find template in Article " + pageid);
				}
			} else { // predominant in SIMPLE Wikipedia
				template = GetTemplatesWithTitleContaining (xmlDoc, "spoken article").FirstOrDefault ();
				if (template != null) {
					Log ("Debug", template.OuterXml);
					var parts = template.GetChildElements ("part").ToList ();
					if (Regex.IsMatch(parts[0].GetChildElement("value").InnerText, "\\.ogg$")) {
						result.audio_file_titles = new string[] { "File:" + parts[0].GetChildElement("value").InnerText };
						if (parts.Count > 1)
							result.date_read = parts[1].GetChildElement("value").InnerText.TrimOrNull();
					}
				}
			}
			return result;
		}

        protected override DateTime? ParseDate(string str)
        {
			var result = base.ParseDate (str);
			if (result == null && str != null) {
				Regex re = new Regex (@"(\d\d\d\d)(\d\d)(\d\d)");
				var match = re.Match(str);
				if (match.Success && match.Groups.Count == 4) {
					result = base.ParseDate (match.Groups[1].Value + "-" + match.Groups[2].Value + "-" + match.Groups[3].Value);
				} 
			}
			return result;
        }
    }
}
