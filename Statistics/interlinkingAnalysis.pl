#!/usr/bin/perl -I /informatik2/nats/home/baumann/perl5/lib/perl5
use strict;
use warnings;
use JSON;
#usage put the name of the speaker and then wikitextfiles as arguments

my %articleNameTable; # names to IDs
my %articleDetailLookupTable; # IDs to names
my %articleLinkedTable; # ID to boolean (via defined)
my $i = 0;
my $speakerName = shift @ARGV;
# convert names to (ad-hoc) article IDs
map  {  m"^(.*)/wiki.txt$"; 
	my $article = $1;
	open JSON, '<', "$article/info.json" or die "could not open JSON file";
	my $json = from_json((join " ", <JSON>), { utf8 => 1 });
	close JSON;
	$articleDetailLookupTable{$i} = {
		name => $article, 
		reader => ($json->{audio_file_parsed}->{reader} or $json->{article_parsed}->{reader})
	};
	$articleNameTable{$article} = $i++;
     } @ARGV; 

my %articles; # to contain lists of article IDs for every article

foreach my $wikitext (@ARGV) {
	$wikitext =~ m"^(.*)/wiki.txt$";
	my $articleID = $articleNameTable{$1};
	next if ($articleDetailLookupTable{$articleID}->{reader} ne $speakerName);
print "$wikitext\n";
	open FILE, '<', $wikitext;
	while (my $articleText = <FILE>) {
		while ($articleText =~ m"\[\[(.*?)[\]\|]"g) {
			my $link = $1;
			$line =~ s/ /_/g;
			if (defined $articleNameTable{$link}) {
				my $linkID = $articleNameTable{$link};
				next if ($articleDetailLookupTable{$linkID}->{reader} ne $speakerName);				
				$articleLinkedTable{$articleID} = 1;
				$articleLinkedTable{$linkID} = 1;
				push @{$articles{$articleID}}, $linkID;
			}
		}
	}
	close FILE;
}
#foreach my $ID (keys %articles) {
#	if (scalar @{$articles{$ID}} < 
#}

print "digraph interlinking {\n";
foreach my $ID (keys %articleLinkedTable) {
	print "$ID [label=\"" . 
		$articleDetailLookupTable{$ID}->{name} . 
		"\"]; # read by: " . 
		$articleDetailLookupTable{$ID}->{reader} . "\n";
}
use List::MoreUtils qw(uniq);
foreach my $articleID (keys %articles) {
	my @links = uniq @{$articles{$articleID}};
	foreach my $link (@links) {
		print "$articleID -> $link;\n";
	}
}
print "}\n";
#use Data::Dumper;
#print Dumper %articles;
