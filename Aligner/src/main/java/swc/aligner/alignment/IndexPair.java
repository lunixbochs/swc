package swc.aligner.alignment;

public class IndexPair
{
    public IndexPair(int left, int right)
    {
        this._left = left;
        this._right = right;
    }
    
    public int getLeft()
    {
        return _left;
    }
    
    public int getRight()
    {
        return _right;
    }

    private int _left;
    private int _right;
    
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof IndexPair)
        {
            IndexPair other = (IndexPair)obj;
            return this._left == other._left && this._right == other._right;
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return _left + (_right>>16);
    }
}