﻿# An attempt at describing and documenting WikiDownloader (unfortunately not by the development wizkid)

The main entry point is Downloader.cs which contains the main() method. WikiDownloader.csproj bundles everything together.

Downloader.cs refers to one of AudioDownloader{,_de,_nl} (for English, German and Dutch recordings) 
or to TextDownloader (e.g. to download good/excellent articles when you want to build a language model) 
to do the actual work, depending on the value of the "retrieve" property in the config.json 

Language specific behaviour in AudioDownloader lie in ParseArticleXML, ParseAudioPageXML, and ParseDate.